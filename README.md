<p align="center">
  <img src="images/forecast_logo.jpg" width="150">
</p>

# Forecast

**Design and Analysis of Massively Parallel Reporter Assays**

Forecast is a Python-based package designed to:

- Generate synthetic MPRA data with custom experimental factors
- Perform statistical inference on MPRA data

Suggestions, comments or requests can be addressed to Pierre-Aurelien Gilliot, ys18223@bristol.ac.uk

## Installation

Please clone the repository via SSH or HTTPs.

e.g. in the appropriate directory, run:

```bash
  git clone https://gitlab.com/Pierre-Aurelien/forecast.git
```

Then execute the following steps:

1. Install the conda environment
   ```bash
   conda env create -f environment.yml
   ```
1. Activate the created conda environment:
   ```bash
   conda activate forecast
   ```
1. Install the package definitions:
   ```bash
   pip install -e .
   ```

## Launching experiments

We here describe the basic command lines to start using FORECAST

1- Simulation of MPRA experiments

The following command will generate a synthetic MPRA dataset using the default gamma-distributed
construct library, with 8 log-spaced bins and 100 million sequencing reads.

```bash
   generate --reads 1e8 auto_bin --bins 8
```

2- Inferring construct performance

```bash
  infer --metadata_path DATA_PATH
```

Here, “DATA_PATH” is the path to the MPRA data

Full description of the tool and command line options is available in:

_Design and Analysis of Massively Parallel Reporter Assays Using FORECAST_, Pierre-Aurelien Gilliot
and Thomas E. Gorochowski, Computational Biology and Machine Learning for Metabolic Engineering and
Synthetic Biology, Methods in Molecular Biology, vol. 2553,
https://doi.org/10.1007/978-1-0716-2617-7_3.

Technical information to derive the estimators and simulation algorithm can be found here:
https://www.overleaf.com/read/zwsdfbbgwtqh

## Code Development

1. Install the conda environment
   ```bash
   conda env create -f environment.yml
   ```
1. Activate the created conda environment:
   ```bash
   conda activate forecast
   ```
1. Install pre-commit hooks
   ```bash
   pre-commit install
   ```
1. Install the package definitions:
   ```bash
   pip install -e .
   ```

## Reproducing experiments from paper

All scripts to reproduce figures are available in the folder figures/, in the supplementrary data.
See below the instructions for each figure:

### FIG 2

All subfigures in Figure3 can be reproduced by executing the cells in the notebook:
figures/fig2/fig2.ipynb

### FIG 3

All subfigures in Figure3 can be reproduced by executing the cells in the notebook:
figures/fig3/fig3.ipynb

### FIG 4a

1. (OPTIONAL) Get data for Experimental-level accuracy using FORECAST. Execute from the FORECAST
   root directory:
   ```bash
   python forecast/analysis/fig4/mistakes.py
   ```
1. Execute cells in jupyter notebook figures/fig4/4a/fig_4a.ipynb

### FIG 4c

1. Execute cells in jupyter notebook figures/fig4/4c/hpo_analysis.ipynb

### FIG 4d

1. (OPTIONAL) Train the Hybrid CNN-BILSTM by executing from the REBECA root directory:
   ```bash
   train --config rebeca/experiments/exp_02
   ```
1. Get performance on test set by executing from the REBECA root directory:
   ```bash
   python rebeca/test.py --config rebeca/experiments/exp_02
   ```
   Confusion matrix and full bootstrap metrics will be availabel in the rebeca/experiments/exp_02
   folder.
