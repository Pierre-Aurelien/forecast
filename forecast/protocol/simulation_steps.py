"""Module for conducting inference on MPRA data."""
import random
from typing import Tuple

import numpy as np
import scipy.stats as stats
from joblib import Parallel, delayed

from forecast.util.simulation import Simulation

# np.random.seed(32)


def sort_cell(i: int, ni: np.ndarray, simulation: Simulation) -> np.ndarray:
    """Sample and bin copies of one genetic construct.

    Args:
        i: construct number
        ni: Sample size for each construct
        simulation: instance of simulation class

    Returns:
        distribution of cells across bins for one construct.
    """
    if simulation.distribution == "lognormal":
        e = np.random.normal(simulation.theta1[i], simulation.theta2[i], ni[i])
    elif simulation.distribution == "gamma":
        e = np.random.gamma(simulation.theta1[i], simulation.theta2[i], ni[i])
    return np.histogram(e, bins=simulation.partition)[0]


def sorting_and_sequencing(simulation: Simulation) -> Tuple[np.ndarray, np.ndarray]:
    """Perform sorting and sequencing steps.

    take as input the number of bins (simulation.bins), the diversity (simulation.diversity),
    size of the library sorted (n), the number of reads to allocate in total (simulation.reads),
    the post sorting amplification step (simulation.ratio_amplification),
    if the library is balanced (BIAS_Library),
    the underlying protein simulation.distribution (gamma or lognormal),
    the fluorescence bounds for the sorting machine (simulation.partition),
    and the parameters of the said simulation.distribution.
    Args:
        simulation: instance of the simulation class

    Returns:
        Return the (simulation.diversity*Bins) matrix resulting from the sequencing
        and the sorting matrix n (number of cell sorted in each bin)
    """
    #### STEP 1 - Draw the ratio p_concentration
    if simulation.bias_library:
        params = np.ones(simulation.diversity) * 0.5
        dirichlet_sample = [random.gammavariate(a, 1) for a in params]
        p_concentration = [v / sum(dirichlet_sample) for v in dirichlet_sample]
        # Sample from the simulation.diversity simplex to get ratios
        # p_concentration=np.ones(simulation.diversity)/simulation.diversity
    else:
        p_concentration = [1 / simulation.diversity] * simulation.diversity

    #### STEP 2 - Draw the sample sizes for each genetic construct
    ni = np.random.multinomial(simulation.size, p_concentration, size=1)[0]
    #### STEP 3 - Compute binning
    nij = Parallel(n_jobs=-1, max_nbytes=None)(
        delayed(sort_cell)(i, ni, simulation) for i in range(simulation.diversity)
    )
    nij = np.array(nij).astype(int)

    #### STEP 4 - PCR amplification
    nij_amplified = np.multiply(nij, simulation.ratio_amplification)

    #### STEP 5 - Compute Reads allocation
    n = np.sum(nij)
    nj = np.sum(nij, axis=0)
    reads = np.floor(
        nj * simulation.reads / (n + 0.001)
    )  # Allocate reads with respect to the number of cells sorted in each bin

    #### STEP 6 - DNA sampling
    sij = np.zeros((simulation.diversity, simulation.bins))

    # Compute ratios& Multinomial sampling
    for j in range(simulation.bins):
        if np.sum(nij_amplified, axis=0)[j] != 0:
            concentration_vector = nij_amplified[:, j] / np.sum(nij_amplified, axis=0)[j]
        else:
            concentration_vector = np.zeros(simulation.diversity)
        sij[:, j] = np.random.multinomial(reads[j], concentration_vector, size=1)
    return (sij, nj)


def sorting(simulation: Simulation) -> np.ndarray:
    """Perform the sorting step.

    Args:
        simulation: instance of the simulation class

    Returns:
        the (simulation.diversity*Bins) matrix resulting from the sorting step

    """
    #### STEP 1 - Draw the ratio p_concentration
    if simulation.bias_library:
        params = np.ones(simulation.diversity)
        dirichlet_sample = [random.gammavariate(a, 1) for a in params]
        p_concentration = [v / sum(dirichlet_sample) for v in dirichlet_sample]
    else:
        p_concentration = [1 / simulation.diversity] * simulation.diversity

    #### STEP 2 - Draw the sample sizes= of each genetic construct
    ni = np.random.multinomial(simulation.size, p_concentration, size=1)[0]

    #### STEP 3 - Compute binning
    nij = Parallel(n_jobs=-1, max_nbytes=None)(
        delayed(sort_cell)(i, ni, simulation) for i in range(simulation.diversity)
    )
    nij = np.array(nij).astype(int)

    return nij


def sequencing(simulation: Simulation, nij: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Perform sequencing step.

    Args:
        simulation: instance of the Simulation class
        nij: Matrix resulting from the sorting step

    Returns:
        Return the (simulation.diversity*Bins) matrix resulting from the sequencing sij
        and the sorting matrix n (number of cell sorted in each bin)

    """
    #### STEP 4 - PCR amplification
    nij_amplified = np.multiply(nij, simulation.ratio_amplification)

    #### STEP 5 - Compute Reads allocation
    n = np.sum(nij)
    nj = np.sum(nij, axis=0)
    reads = np.floor(
        nj * simulation.reads / (n + 0.001)
    )  # Allocate reads with respect to the number of cells srted in each bin
    #### STEP 6 - DnA sampling

    sij = np.zeros((simulation.diversity, simulation.bins))

    # Compute ratios& Multinomial sampling
    for j in range(simulation.bins):
        if np.sum(nij_amplified, axis=0)[j] != 0:
            concentration_vector = nij_amplified[:, j] / np.sum(nij_amplified, axis=0)[j]
        else:
            concentration_vector = np.zeros(simulation.diversity)
        sij[:, j] = np.random.multinomial(reads[j], concentration_vector, size=1)
    return sij, nj


def sorting_and_sequencing_probability_mass(
    simulation: Simulation,
) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    """Perform sorting and sequencing steps.

    take as input the number of bins (simulation.bins), the diversity (simulation.diversity),
    size of the library sorted (n), the number of reads to allocate in total (simulation.reads),
    the post sorting amplification step (simulation.ratio_amplification),
    if the library is balanced (BIAS_Library),
    the underlying protein simulation.distribution (gamma or lognormal),
    the fluorescence bounds for the sorting machine (simulation.partition),
    and the parameters of the said simulation.distribution.
    Args:
        simulation: instance of the simulation class

    Returns:
        Return the (simulation.diversity*Bins) matrix resulting from the sequencing, the sorting matrix n (number of cell sorted in each bin) and the probability mass matrix
    """

    def prob_mass(i: int, j: int) -> np.ndarray:
        """Returns probability mass in each bin of one genetic construct.

        Args:
            i: construct number
            ni: bin number
            simulation: instance of simulation class

        Returns:
            truncated probaility mass across bins for one construct.
        """
        if simulation.distribution == "lognormal":
            element_matrix = stats.norm.cdf(
                simulation.partition[j + 1], loc=simulation.theta1[i], scale=simulation.theta2[i]
            ) - stats.norm.cdf(
                simulation.partition[j], loc=simulation.theta1[i], scale=simulation.theta2[i]
            )
        elif simulation.distribution == "gamma":
            element_matrix = stats.gamma.cdf(
                simulation.partition[j + 1], a=simulation.theta1[i], scale=simulation.theta2[i]
            ) - stats.gamma.cdf(
                simulation.partition[j], a=simulation.theta1[i], scale=simulation.theta2[i]
            )
        return element_matrix

    #### STEP 1 - Draw the ratio p_concentration
    if simulation.bias_library:
        params = np.ones(simulation.diversity) * 0.5
        dirichlet_sample = [random.gammavariate(a, 1) for a in params]
        p_concentration = [v / sum(dirichlet_sample) for v in dirichlet_sample]
        # Sample from the simulation.diversity simplex to get ratios
        # p_concentration=np.ones(simulation.diversity)/simulation.diversity
    else:
        p_concentration = [1 / simulation.diversity] * simulation.diversity

    #### STEP 2 - Draw the sample sizes of each genetic construct
    ni = np.random.multinomial(simulation.size, p_concentration, size=1)[0]

    #### STEP 3 - Compute binning
    nij = Parallel(n_jobs=-1, max_nbytes=None)(
        delayed(sort_cell)(i, ni, simulation) for i in range(simulation.diversity)
    )
    nij = np.array(nij).astype(int)

    ### STEP 3.5- Compute ratios
    qij = np.fromfunction(prob_mass, (simulation.diversity, simulation.bins), dtype=int)

    #### STEP 4 - PCR amplification
    nij_amplified = np.multiply(nij, simulation.ratio_amplification)

    #### STEP 5 - Compute Reads allocation
    n = np.sum(nij)
    nj = np.sum(nij, axis=0)
    reads = np.floor(
        nj * simulation.reads / (n + 0.001)
    )  # Allocate reads with respect to the number of cells sorted in each bin

    #### STEP 6 - DNA sampling
    sij = np.zeros((simulation.diversity, simulation.bins))

    # Compute ratios& Multinomial sampling
    for j in range(simulation.bins):
        if np.sum(nij_amplified, axis=0)[j] != 0:
            concentration_vector = nij_amplified[:, j] / np.sum(nij_amplified, axis=0)[j]
        else:
            concentration_vector = np.zeros(simulation.diversity)
        sij[:, j] = np.random.multinomial(reads[j], concentration_vector, size=1)
    return (sij, nj, qij)


# def prob_mass(i: int, j: int, simulation: Simulation) -> np.ndarray:
#     """Returns probability mass in each bin of one genetic construct.
#
#     Args:
#         i: construct number
#         ni: bin number
#         simulation: instance of simulation class
#
#     Returns:
#         truncated probaility mass across bins for one construct.
#     """
#     if simulation.distribution == "lognormal":
#         element_matrix = stats.norm.cdf(
#             simulation.partition[j + 1], loc=simulation.theta1[i], scale=simulation.theta2[i]
#         ) - stats.norm.cdf(
#             simulation.partition[j], loc=simulation.theta1[i], scale=simulation.theta2[i]
#         )
#     elif simulation.distribution == "gamma":
#         element_matrix = stats.gamma.cdf(
#             simulation.partition[j + 1], a=simulation.theta1[i], scale=simulation.theta2[i]
#         ) - stats.gamma.cdf(
#             simulation.partition[j], a=simulation.theta1[i], scale=simulation.theta2[i]
#         )
#     return element_matrix
