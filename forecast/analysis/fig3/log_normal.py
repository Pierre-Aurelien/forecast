"""Module to save graphs for factorial experiments."""
import os

import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from matplotlib import rcParams

# rcParams dict
rcParams["axes.labelsize"] = 9
rcParams["xtick.labelsize"] = 9
rcParams["ytick.labelsize"] = 9
rcParams["legend.fontsize"] = 9
# rcParams['font.family'] = 'serif'
# rcParams['font.serif'] = ['Computer Modern Roman']
# rcParams['text.usetex'] = True
rcParams["figure.figsize"] = 7.3, 4.2


df = pd.read_csv("complete.csv")

df["normalised_reads"] = round(df["seq"] / 1000).astype(int)  # type: ignore[attr-defined]
df["normalised_cells"] = round(df["size"] / 1000).astype(int)  # type: ignore[attr-defined]

summary_statistics = ["mu", "sigma", "fluo_mu", "fluo_sigma"]
for bins in [6, 8, 10, 12, 14, 16, 18, 20, 32]:
    os.mkdir(f"{bins}_bins")
    for ss in summary_statistics:
        observable = f"mape_{ss}"
        fig, ax = plt.subplots(figsize=(15, 10))
        sns.lineplot(
            data=df[df["bins"] == bins],
            x="normalised_reads",
            y=f"{observable}_mle",
            hue="normalised_cells",
            linewidth=3,
            palette=sns.color_palette("Greys", 9),
        )
        sns.lineplot(
            data=df[df["bins"] == bins],
            x="normalised_reads",
            y=f"{observable}_mom",
            hue="normalised_cells",
            palette=sns.color_palette("Blues", 9),
        )
        ax.legend(bbox_to_anchor=(1.01, 0.8), borderaxespad=0, frameon=False, fontsize=15)
        ax.set(xscale="log")
        if observable == "mape_fluo_sigma":
            ax.set(yscale="log")
        plt.title(
            f"MAPE of {ss} accross simulations and inference methods, with {bins} bins.",
            fontsize=15,
        )
        sns.despine()
        fig.tight_layout(pad=0.1)  # Make the figure use all available whitespace
        plt.savefig(f"{bins}_bins/mape_{ss}_{bins}_bins.pdf", transparent=True)
# fig.tight_layout(pad=0.1)  # Make the figure use all available whitespace
# plt.savefig('mape.pdf', transparent=True)
# plt.show()
