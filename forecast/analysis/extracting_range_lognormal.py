"""Script to perform analysis of range data."""
import argparse
from datetime import datetime
from pathlib import Path

import numpy as np
import pandas as pd
from joblib import Parallel, delayed
from numpy import nan

from forecast.util.stat import get_ape, get_empirical_wd, log_moments, visibility


def time_created(path: Path) -> float:
    """Indicates the time the path was created as in https://www.unixtimestamp.com/ .

    Args:
        path: directory path

    Returns:
        Unix time
    """
    return path.stat().st_atime


def parse_args():
    """Parse arguments."""
    parser = argparse.ArgumentParser(description="Extracting range of experiments.")
    parser.add_argument(
        "--simulation_path",
        type=str,
        help="the simulation path folder.",
    )

    parser.add_argument(
        "--num_workers", type=int, default=-1, help="Number of workers in parallel."
    )
    parser.add_argument(
        "--verbose", type=int, default=1, help="progress messages for multi-parallel processing."
    )
    parser.add_argument(
        "--f_amp",
        type=int,
        default=0,
        choices=range(1, int(1e6)),
        metavar="PARAMETER",
        help="Ratio fluorescence/protein.",
    )
    parser.add_argument(
        "--output_path",
        type=Path,
        default="out/extract_" + datetime.now().strftime("%Y%m%d-%H%M%S"),
        help="Path for the output folder.",
    )
    parser.add_argument(
        "--wd",
        type=bool,
        default=False,
        help="Whether or not to compute Wasserstein distance",
    )
    args = parser.parse_args()
    return args


def process_data(df, library, num_workers, args, nb_features):  # noqa  CCR001
    """Extract summary information for each simulation.

    Args:
        df: dataframe containing inference results for each construct, corresponding to one simulation.
        library:dataframe with library of parameters.
        num_workers:number of cpus

    Returns:
        summary information
    """

    def wd_parallel(i, df, inference, fluo):
        row = df.iloc[i]
        if fluo:
            to_compute = get_empirical_wd(
                "lognormal",
                row["mu"],
                row["sigma"],
                row[f"mu_{inference}"],
                row[f"sigma_{inference}"],
            )
        else:
            to_compute = get_empirical_wd(
                "normal", row["mu"], row["sigma"], row[f"mu_{inference}"], row[f"sigma_{inference}"]
            )
        return to_compute

    # df = df.rename(columns={"mean": "mu_mom", "st_dev": "sigma_mom"})
    inference_method = ["mle", "mom"]
    summary_statistics = ["mu", "sigma", "fluo_mu", "fluo_sigma"]

    df["mu"] = library["mu"]
    df["sigma"] = library["sigma"]
    df["visibility"] = df.apply(
        lambda row: visibility(row["mu"], row["sigma"], 1e5, "lognormal"), axis=1
    )
    # Filtering step
    df = df[df["visibility"] > 0.5]
    df = df[df["score"] < 0.7]
    df = df[df["inference_grade"] == 1]
    df = df[(df["mu_mle"] > df["mu_se"]) & (df["sigma_mle"] > df["sigma_se"])]
    df["fluo_mu"], df["fluo_sigma"] = log_moments(df["mu"], df["sigma"])
    for i_m in inference_method:
        df[f"fluo_mu_{i_m}"], df[f"fluo_sigma_{i_m}"] = log_moments(
            df[f"mu_{i_m}"], df[f"sigma_{i_m}"]
        )
    # Filter outliers
    cols = ["fluo_sigma_mom", "fluo_mu_mom", "fluo_sigma_mle", "fluo_mu_mle"]  # one or more

    q1 = df[cols].quantile(0.25)
    q3 = df[cols].quantile(0.75)
    iqr = q3 - q1

    df = df[~((df[cols] < (q1 - 1.5 * iqr)) | (df[cols] > (q3 + 1.5 * iqr))).any(axis=1)]

    if len(df) == 0:
        aggregate_results = [nan] * nb_features
    else:
        # Computing summary statistics
        for s_s in summary_statistics:
            for i_m in inference_method:
                df[f"ape_{s_s}_{i_m}"] = get_ape(df[f"{s_s}"], df[f"{s_s}_{i_m}"])
        if args.wd:
            for i_m in inference_method:
                df[f"wd_{i_m}"] = np.array(
                    Parallel(n_jobs=num_workers, verbose=1, max_nbytes=None)(
                        delayed(wd_parallel)(i, df, f"{i_m}", False) for i in range(len(df))
                    )
                )
                df[f"fluo_wd_{i_m}"] = np.array(
                    Parallel(n_jobs=num_workers, verbose=1, max_nbytes=None)(
                        delayed(wd_parallel)(i, df, f"{i_m}", True) for i in range(len(df))
                    )
                )
        # Aggregating metrics
        aggregate_results = {"nb_filtered_constructs": len(df)}
        for i_m in inference_method:
            for s_s in summary_statistics:
                aggregate_results[f"mape_{s_s}_{i_m}"] = df[f"ape_{s_s}_{i_m}"].mean()
                aggregate_results[f"sape_{s_s}_{i_m}"] = df[f"ape_{s_s}_{i_m}"].std()
            if args.wd:
                aggregate_results[f"mape_wd_{i_m}"] = df[f"wd_{i_m}"].mean()
                aggregate_results[f"sape_wd_{i_m}"] = df[f"wd_{i_m}"].std()
                aggregate_results[f"mape_fluo_wd_{i_m}"] = df[f"fluo_wd_{i_m}"].mean()
                aggregate_results[f"sape_fluo_wd_{i_m}"] = df[f"fluo_wd_{i_m}"].std()
    return aggregate_results


def main():  # noqa: CCR001
    """Main script."""
    # parse args
    args = parse_args()
    output_path = Path(args.output_path)
    output_path.mkdir(parents=True, exist_ok=True)

    # library data
    library = pd.read_csv(Path("forecast/data") / "lognormal/library_lognormal.csv")
    library["mu"] += args.f_amp
    inference_method = ["mle", "mom"]
    summary_statistics = ["mu", "sigma", "fluo_mu", "fluo_sigma"]
    aggregate_method = ["mape", "sape"]
    mape_list = [
        f"{a_m}_{s_s}_{i_m}"
        for i_m in inference_method
        for s_s in summary_statistics
        for a_m in aggregate_method
    ]
    wd_list = [f"{a_m}_wd_{i_m}" for i_m in inference_method for a_m in aggregate_method]
    fluo_wd_list = [f"fluo_{a_m}_wd_{i_m}" for i_m in inference_method for a_m in aggregate_method]
    context_list = [
        "fmax",
        "distribution",
        "diversity",
        "bias",
        "rep",
        "seq",
        "bins",
        "size",
        "pcr_amp",
        "f_amp",
    ]
    nb_new_features = len(mape_list + (wd_list + fluo_wd_list) * args.wd)

    path_simulation = Path(f"{args.simulation_path}")
    list_data = []

    all_paths = list(path_simulation.glob("results_*"))
    few_index = len(all_paths) // 10  # Extract a sample

    for path_result in all_paths[:few_index]:
        ctxt = path_result.stem
        list_context = ctxt[ctxt.find("(") + 1 : ctxt.find(")")].replace("'", "").split(",")
        dict_summary = process_data(
            pd.read_csv(path_result), library, args.num_workers, args, nb_new_features
        )
        list_data.append(list_context + list(dict_summary.values()))

    df_first = pd.DataFrame(list_data, columns=context_list + list(dict_summary.keys()))
    df_first.to_csv(output_path / "first.csv", index=False)

    # End extraction
    for path_result in all_paths[few_index:]:
        ctxt = path_result.stem
        list_context = ctxt[ctxt.find("(") + 1 : ctxt.find(")")].replace("'", "").split(",")
        dict_summary = process_data(
            pd.read_csv(path_result), library, args.num_workers, args, nb_new_features
        )
        list_data.append(list_context + list(dict_summary.values()))

    df_summary = pd.DataFrame(list_data, columns=context_list + list(dict_summary.keys()))
    df_summary.to_csv(output_path / "complete.csv", index=False)
