"""width and ci."""
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

matplotlib.rcParams["pdf.fonttype"] = 42
matplotlib.rcParams["ps.fonttype"] = 42

df_truth = pd.read_csv("forecast/data/gamma/library_gamma.csv")

df_truth["b"] = 10 * df_truth["b"]
ground_truth = df_truth.to_numpy()[:6, :]


def get_info(probit, construct, raw_data):
    """Get info for width and ci."""
    # Filter out weird constructs
    # First, make sure the score is 1
    aux = raw_data[:, construct, :]
    aux = aux[aux[:, 2] < 2]  # score must be <1
    aux = aux[aux[:, 1] < aux[:, 0]]  # se must be smaller than the estimate

    number_trials = len(aux)

    coverage_counter = 0
    for i in range(number_trials):
        if np.abs(ground_truth[construct, 0] - aux[i, 0]) <= probit * aux[i, 1]:
            coverage_counter += 1
    print(f"{coverage_counter}/{number_trials}={np.round(coverage_counter / number_trials, 3)}")
    return np.mean(aux[:, 1]), np.round(coverage_counter / number_trials, 3)


if __name__ == "__main__":
    fig, ax = plt.subplots(1, 1, figsize=(6, 6))
    for seq in [1e5, 1e6, 1e7]:
        coverage = []
        width = []
        for size in [1e5, 1e6, 1e7]:
            a_data = np.load(f"a_seq_size_({seq}, {size}).npy")
            w, c = get_info(1.96, 0, a_data)
            coverage.append(c)
            width.append(w)
        ax.plot([1e5, 1e6, 1e7], width, marker="o", label=f"seq {seq:.1E}")
    plt.legend(loc="upper left")
    plt.savefig("a_width.pdf", transparent=True)
    plt.show()

    # plt.savefig(f'a_seq_size_({seq}, {size}).pdf', transparent=True)
    # plt.show()
# def plot_coverage_b(probit, construct, raw_data, theta, filtering):
#     # Filter out weird constructs
#     # First, make sure the score is 1
#     aux = raw_data[:, construct, :]
#     aux = aux[aux[:, 2] < 2]  # score must be <1
#     aux[aux[:, 1] < aux[:, 0]]  # se must be smaller than the estimate
#
#     number_trials = len(aux)
#
#     coverage_counter = 0
#     fig, ax = plt.subplots(1, 1, figsize=(6, 8))
#     y_coord = np.arange(number_trials)
#     for i in range(number_trials):
#         if np.abs(ground_truth[construct, theta] - raw_data[i, construct, theta]) <= probit * raw_data[
#             i, construct, 2 + theta]:
#             # interval contains p
#             ax.errorbar(raw_data[i, construct, theta], y_coord[i], lolims=True,
#                         xerr=probit * raw_data[i, construct, 2 + theta], yerr=0.0, linestyle='', c='grey')
#             coverage_counter += 1
#         else:
#             # interval does not contain p
#             ax.errorbar(raw_data[i, construct, theta], y_coord[i], lolims=True,
#                         xerr=probit * raw_data[i, construct, 2 + theta], yerr=0.0, linestyle='', c='tab:red')
#     ax.axvline(ground_truth[construct, theta], color='black')
#     plt.ylabel('Simulation ', fontsize=16)
#     plt.xlabel('parameter value', fontsize=20)
#     plt.title(f'{coverage_counter}/{number_trials}={np.round(coverage_counter / number_trials, 2)}')
#
#     custom = [Line2D([], [], marker='.', markersize=20, color='grey', linestyle='None'),
#               Line2D([], [], marker='.', markersize=20, color='tab:red', linestyle='None')]
#     plt.legend(handles=custom, title='Parameter in CI', labels=['Yes', 'No'], loc='upper right', fontsize=10)
#     print(f'{coverage_counter}/{number_trials}={np.round(coverage_counter / number_trials, 2)}')
# #     return fig
