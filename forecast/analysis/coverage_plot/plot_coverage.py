"""Plot coverage."""
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from matplotlib.lines import Line2D

matplotlib.rcParams["pdf.fonttype"] = 42
matplotlib.rcParams["ps.fonttype"] = 42

df_truth = pd.read_csv("forecast/data/gamma/library_gamma.csv")

df_truth["b"] = 10 * df_truth["b"]
ground_truth = df_truth.to_numpy()[:6, :]


def plot_coverage_a(probit, construct, raw_data, seq_reads, size_cells):
    """Get coverage info."""
    # Filter out weird constructs
    # First, make sure the score is 1
    aux = raw_data[:, construct, :]
    aux = aux[aux[:, 2] < 2]  # score must be <1
    aux = aux[aux[:, 1] < aux[:, 0]]  # se must be smaller than the estimate

    number_trials = len(aux)

    coverage_counter = 0
    figure, ax = plt.subplots(1, 1, figsize=(6, 8))
    y_coord = np.arange(number_trials)
    for i in range(number_trials):
        if np.abs(ground_truth[construct, 0] - aux[i, 0]) <= probit * aux[i, 1]:
            # interval contains p
            ax.errorbar(
                aux[i, 0],
                y_coord[i],
                lolims=True,
                xerr=probit * aux[i, 1],
                yerr=0.0,
                linestyle="",
                c="grey",
            )
            coverage_counter += 1
        else:
            # interval does not contain p
            ax.errorbar(
                aux[i, 0],
                y_coord[i],
                lolims=True,
                xerr=probit * aux[i, 1],
                yerr=0.0,
                linestyle="",
                c="tab:red",
            )
    ax.axvline(ground_truth[construct, 0], color="black")
    plt.ylabel("Simulation number ", fontsize=16)
    plt.xlabel("parameter value", fontsize=16)
    plt.title(
        f"Coverage for seq,size of {seq_reads:.1E}, {size_cells:.1E} is {coverage_counter}/{number_trials}={np.round(coverage_counter / number_trials, 2)}"
    )

    custom = [
        Line2D([], [], marker=".", markersize=20, color="grey", linestyle="None"),
        Line2D([], [], marker=".", markersize=20, color="tab:red", linestyle="None"),
    ]
    plt.legend(
        handles=custom,
        title="Parameter in CI",
        labels=["Yes", "No"],
        loc="upper right",
        fontsize=10,
    )
    print(f"{coverage_counter}/{number_trials}={np.round(coverage_counter / number_trials, 3)}")
    return figure


if __name__ == "__main__":
    reads = [1e5, 1e6, 1e7]
    cells = [1e5, 1e6, 1e7]
    for (
        seq,
        size,
    ) in [(seq1, size1) for seq1 in reads for size1 in cells]:
        a_data = np.load(f"a_seq_size_({seq}, {size}).npy")
        fig = plot_coverage_a(1.96, 0, a_data, seq, size)
        plt.savefig(f"a_seq_size_({seq}, {size}).pdf", transparent=True)
        # plt.show()
# def plot_coverage_b(probit, construct, raw_data, theta, filtering):
#     # Filter out weird constructs
#     # First, make sure the score is 1
#     aux = raw_data[:, construct, :]
#     aux = aux[aux[:, 2] < 2]  # score must be <1
#     aux[aux[:, 1] < aux[:, 0]]  # se must be smaller than the estimate
#
#     number_trials = len(aux)
#
#     coverage_counter = 0
#     fig, ax = plt.subplots(1, 1, figsize=(6, 8))
#     y_coord = np.arange(number_trials)
#     for i in range(number_trials):
#         if np.abs(ground_truth[construct, theta] - raw_data[i, construct, theta]) <= probit * raw_data[
#             i, construct, 2 + theta]:
#             # interval contains p
#             ax.errorbar(raw_data[i, construct, theta], y_coord[i], lolims=True,
#                         xerr=probit * raw_data[i, construct, 2 + theta], yerr=0.0, linestyle='', c='grey')
#             coverage_counter += 1
#         else:
#             # interval does not contain p
#             ax.errorbar(raw_data[i, construct, theta], y_coord[i], lolims=True,
#                         xerr=probit * raw_data[i, construct, 2 + theta], yerr=0.0, linestyle='', c='tab:red')
#     ax.axvline(ground_truth[construct, theta], color='black')
#     plt.ylabel('Simulation ', fontsize=16)
#     plt.xlabel('parameter value', fontsize=20)
#     plt.title(f'{coverage_counter}/{number_trials}={np.round(coverage_counter / number_trials, 2)}')
#
#     custom = [Line2D([], [], marker='.', markersize=20, color='grey', linestyle='None'),
#               Line2D([], [], marker='.', markersize=20, color='tab:red', linestyle='None')]
#     plt.legend(handles=custom, title='Parameter in CI', labels=['Yes', 'No'], loc='upper right', fontsize=10)
#     print(f'{coverage_counter}/{number_trials}={np.round(coverage_counter / number_trials, 2)}')
# #     return fig
