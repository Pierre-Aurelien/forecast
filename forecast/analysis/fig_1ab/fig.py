"""Script to generate fig 1a-b of publication."""
import os
from pathlib import Path

import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns

from forecast.util.stat import ab_to_ms

matplotlib.rcParams["pdf.fonttype"] = 42
matplotlib.rcParams["ps.fonttype"] = 42
plt.rcParams.update({"font.size": 6})
linewidth = 1
s = 2

path = Path.cwd()
path_out = path / "forecast/analysis/fig_1ab/out"

# os.chdir(path)
bins = 10
f_max = 1e5
reads = 1e7
f_amp = 10
size = 1e6
# subprocess.call(['generate','--reads','1e7','--f_amp',' 10','--size','1e6','auto_bin', '--bins', '8' ,'--f_max', '1e5'])
# cmd = [os.path.realpath('./sa.py'), '--track', '12']
os.system(
    f" generate auto_bin --bins {bins} --f_max {f_max} --reads {reads} --f_amp {f_amp} --size {size}"
)
# generate auto_bin --bins 8 --f_max 1e5 --reads 1e7 --f_amp 10 --size 1e6
os.system(
    f"infer auto_bin --last_index all --out_path 'forecast/analysis/fig_1ab/out' --f_max {f_max}"
)
df = pd.read_csv(path / "forecast/data/gamma/library_gamma.csv")
print("The diversity of the library is: ", len(df))
theta1 = df.iloc[:, 0].to_numpy()
theta2 = f_amp * df.iloc[:, 1].to_numpy()  # Fluorescence protein ratio

cm = 1 / 2.54  # centimeters in inches

fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(2 * 7.5 * cm, 7.5 * cm))
# p.drop(p.index[[76,671]],inplace=True)

p = pd.read_csv(path / "forecast/analysis/fig_1ab/out/results.csv")
p["mean_gt"], p["st_dev_gt"] = ab_to_ms(df.iloc[:, 0], f_amp * df.iloc[:, 1])
p["mu_mle"], p["sigma_mle"] = ab_to_ms(p["a_mle"], p["b_mle"])
print(p.head())
p = p[p["score"] <= 0.7]  # Keep constructs not on the border
p = p[p["inference_grade"] == 1]  # Keep constructs with good shape
p = p[p["a_se"] < p["a_mle"]]
p = p[p["b_se"] < p["b_mle"]]

X_mu = p.sort_values("mean_gt", ascending=True)["mean_gt"]
Y_mu = p.sort_values("mean_gt", ascending=True)["mu_mle"]
Z_mu = p.sort_values("mean_gt", ascending=True)["mean"]
X_sigma = p.sort_values("st_dev_gt", ascending=True)["st_dev_gt"]
Y_sigma = p.sort_values("st_dev_gt", ascending=True)["sigma_mle"]
Z_sigma = p.sort_values("st_dev_gt", ascending=True)["st_dev"]

ax1.scatter(X_mu, Y_mu, s=s, c="tab:grey", label="ML estimate")
ax1.scatter(X_mu, Z_mu, s=s, c=sns.color_palette("Blues_d")[3], label="MOM estimate")
ax1.plot(X_mu, X_mu, c="black", label="y=x", linewidth=linewidth, linestyle="dashed")
ax1.legend()
ax1.set(xlabel="mean Fluorescence", ylabel="Estimated mean Fluorescence")
# plt.title('')
ax1.legend(frameon=False, markerscale=2)
ax1.set_yscale("log")
ax1.set_xscale("log")
ax2.scatter(X_sigma, Y_sigma, s=s, c="tab:grey", label="ML estimate")
ax2.scatter(X_sigma, Z_sigma, s=s, c=sns.color_palette("Blues_d")[3], label="MOM estimate")
ax2.plot(X_sigma, X_sigma, c="black", label="y=x", linewidth=linewidth, linestyle="dashed")
ax2.legend()
ax2.set(
    xlabel=" Fluorescence standard deviation", ylabel="Estimated fluorescence standard deviation"
)
ax2.legend(frameon=False, markerscale=2)
ax2.set_yscale("log")
ax2.set_xscale("log")
sns.despine()
# ax2.set_title(f'Ranking parameter influence,output is {ss}', fontsize=20)
plt.savefig(
    Path(path_out) / f"fig1ab_bin{bins}_famp{f_amp}.pdf",
    transparent=True,
    bbox_inches="tight",
    dpi=600,
)
