"""Main script to get upper limit on experiment-level accuracy."""
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from joblib import Parallel, delayed

from forecast.protocol.simulation_steps import sorting, sorting_and_sequencing_probability_mass
from forecast.util.simulation import Simulation


def main():
    """Main script."""
    # diversity=32 000        [50 ,10 ,20 ,30, 40 ,50, 100, 200, 300 ,500 ,1000 ,2000 ,3000 ,5000]
    # reads=858884 860 000
    # size= 3 000 000
    # bins=8
    scale_size = [1, 5, 10, 15] + list(range(20, 80, 20)) + [72] + list(range(80, 210, 30))
    scale_reads = list(range(1, 60, 2)) + list(range(60, 210, 5))
    bins = 8
    replicates = int(1)
    results = np.zeros((replicates, len(scale_size), len(scale_reads)))
    f_max = 1e6
    # Loading library data
    df = pd.read_csv("forecast/data/lognormal/library_lognormal.csv")

    # Extract distribution parameters
    nij_percent_real = [
        0.2632708,
        0.166259547,
        0.127467656,
        0.122175345,
        0.113685678,
        0.112422203,
        0.059105519,
        0.035613253,
    ]
    theta1 = df.iloc[:, 0].to_numpy()
    theta2 = df.iloc[:, 1].to_numpy()
    fluo_boundary = np.logspace(0, np.log10(f_max), bins + 1)
    fluo_boundary[0] = 0
    diversity = len(theta1)

    for rep in range(replicates):
        optim = []
        range_fluo = [1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 0.5, 1, 2, 5, 10, 50, 100]
        # set fluorescence parameter to match as musch as possible the cell binning distribution
        for fluo in range_fluo:
            my_simulation = Simulation(
                bins=bins,
                diversity=len(theta1),
                size=1e5,
                reads=1e5,
                fmax=f_max,
                distribution="lognormal",
                ratio_amplification=1e3,
                theta1=theta1 + np.log(fluo),
                theta2=theta2,
                bias_library=True,
                fluo_boundary=fluo_boundary,
            )
            nj = sorting(my_simulation).sum(axis=0)
            print("the sorting is", fluo, nj / sum(nj))
            optim.append(np.linalg.norm(x=(nj / sum(nj) - nij_percent_real), ord=2))

        fluo_param_optim = range_fluo[optim.index(min(optim))]
        print("the fluo is", fluo_param_optim)
        theta1 += np.log(fluo_param_optim)

        def accuracy_limit(i, j):
            my_simulation = Simulation(
                bins=bins,
                diversity=len(theta1),
                size=scale_size[i] * len(theta1),
                reads=scale_reads[j] * len(theta1),
                fmax=f_max,
                distribution="lognormal",
                ratio_amplification=1e3,
                theta1=theta1,
                theta2=theta2,
                bias_library=True,
                fluo_boundary=fluo_boundary,
            )

            # Generate Flow-seq data
            sequencing_matrix, _, ratios = sorting_and_sequencing_probability_mass(my_simulation)
            df1 = pd.DataFrame(ratios)
            df1 = df1.eq(df1.where(df1 != 0).max(1), axis=0).astype(int)
            df1["label"] = df1.apply(lambda x: x.index.get_loc(x.idxmax()) + 1, axis=1)

            # Compute bin mode
            df2 = pd.DataFrame(sequencing_matrix)
            df2 = df2.eq(df2.where(df2 != 0).max(1), axis=0).astype(int)
            df2["label"] = df2.apply(lambda x: x.index.get_loc(x.idxmax()) + 1, axis=1)
            # Compute the difference
            diff = (df2["label"] - df1["label"]).astype(bool).sum()

            max_accuracy = 1 - diff / diversity
            return max_accuracy

        for i in range(len(scale_size)):
            max_accuracy_j = Parallel(n_jobs=-1, max_nbytes=None)(
                delayed(accuracy_limit)(i, j) for j in range(len(scale_reads))
            )
            max_accuracy_j = np.array(max_accuracy_j)
            print(max_accuracy_j)
            results[rep, i, :] = max_accuracy_j
        print(results[rep].shape)

    # plot it!:
    # sns.axes_style()
    # sns.set_style("whitegrid")
    # sns.distplot()
    fig_dims = (20, 8)
    _, ax = plt.subplots(figsize=fig_dims)
    palette = sns.color_palette("Blues", len(scale_size))
    for i, _ in enumerate(scale_size):
        if scale_size[i] == 72:
            thickness = 4
        else:
            thickness = 1
        ax.plot(
            scale_reads, np.mean(results[:, i, :], axis=0), color=palette[i], linewidth=thickness
        )

    plt.legend()
    ax.legend(frameon=False, fontsize=16, markerscale=1.5)
    plt.xlabel("Normalised reads")
    # plt.axvline(x=30, color="k", linestyle='--')
    plt.ylabel("Accuracy upper limit", fontsize=16)
    plt.title("Experimental-level accuracy")
    plt.savefig(
        "out/experiment_accuracy_log_reads.pdf",
        transparent=True,
        bbox_inches="tight",
        dpi=100,
    )
    np.savetxt(
        "out/upper_limit_log.csv",
        np.mean(results, axis=0),
        comments="",
        delimiter=",",
        header=",".join([f"{x}" for x in scale_reads]),
    )


if __name__ == "__main__":
    main()
