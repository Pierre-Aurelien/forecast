"""Script to perform analysis of range data."""
# import argparse
# from datetime import datetime
# from pathlib import Path
#
# import matplotlib.pyplot as plt
# import numpy as np
# import pandas as pd
# import seaborn as sns
# from joblib import Parallel, delayed
# from numpy import nan
#
# from forecast.util.stat import ab_to_ms, get_ape, get_empirical_wd
#
#
# def time_created(path: Path) -> float:
#     """Indicates the time the path was created as in https://www.unixtimestamp.com/ .
#
#     Args:
#         path: directory path
#
#     Returns:
#         Unix time
#     """
#     return path.stat().st_atime
#
#
# def parse_args():
#     """Parse arguments."""
#     parser = argparse.ArgumentParser(description="Extracting range of experiments.")
#     parser.add_argument(
#         "--simulation_directory",
#         type=str,
#         help="Name of the simulation directory inside the out/ directory.",
#     )
#
#     parser.add_argument(
#         "--num_workers", type=int, default=-1, help="Number of workers in parallel."
#     )
#     parser.add_argument(
#         "--verbose", type=int, default=1, help="progress messages for multi-parallel processing."
#     )
#     parser.add_argument(
#         "--f_amp",
#         type=int,
#         default=10,
#         choices=range(1, int(1e6)),
#         metavar="PARAMETER",
#         help="Ratio fluorescence/protein.",
#     )
#     parser.add_argument(
#         "--output_path",
#         type=Path,
#         default="out/extract_" + datetime.now().strftime("%Y%m%d-%H%M%S"),
#         help="Path for the output folder.",
#     )
#     parser.add_argument(
#         "--wd",
#         type=bool,
#         default=False,
#         help="Whether or not to compute Wasserstein distance",
#     )
#     args = parser.parse_args()
#     return args
#
#
# def process_data(df, library, num_workers, args, nb_features):  # noqa  CCR001
#     """Extract summary information for each simulation.
#
#     Args:
#         df: dataframe containing inference results for each construct, corresponding to one simulation.
#         library:dataframe with library of parameters.
#         num_workers:number of cpus
#
#     Returns:
#         summary information
#     """
#
#     def wd_parallel(i, df, inference):
#         row = df.iloc[i]
#         return get_empirical_wd(
#             "gamma", row["a"], row["b"], row[f"a_{inference}"], row[f"b_{inference}"]
#         )
#
#     inference_method = ["mle", "mom"]
#     summary_statistics = ["a", "b", "fluo_mu", "fluo_sigma"]
#
#     df["a"] = library["a"]
#     df["b"] = library["b"]
#     # Filtering step
#     df = df[df["score"] < 0.7]
#     df = df[df["inference_grade"] == 1]
#     df = df[(df["a_mle"] > df["a_se"]) & (df["b_mle"] > df["b_se"])]
#
#     df["fluo_mu"], df["fluo_sigma"] = ab_to_ms(df["a"], df["b"])
#     for i_m in inference_method:
#         df[f"fluo_mu_{i_m}"], df[f"fluo_sigma_{i_m}"] = ab_to_ms(df[f"a_{i_m}"], df[f"b_{i_m}"])
#
#     # Filter outliers
#     cols = ["fluo_sigma_mom", "fluo_mu_mom", "fluo_sigma_mle", "fluo_mu_mle"]  # one or more
#
#
#     # q1 = df[cols].quantile(0.25)
#     # q3 = df[cols].quantile(0.75)
#     # iqr = q3 - q1
#     #
#     # df = df[~((df[cols] < (q1 - 1.5 * iqr)) | (df[cols] > (q3 + 1.5 * iqr))).any(axis=1)]
#     #
#     # if len(df) == 0:
#     #     aggregate_results = [nan] * nb_features
#     # else:
#     #     # Computing summary statistics
#     #     for s_s in summary_statistics:
#     #         for i_m in inference_method:
#     #             df[f"ape_{s_s}_{i_m}"] = get_ape(df[f"{s_s}"], df[f"{s_s}_{i_m}"])
#     #     if args.wd:
#     #         for i_m in inference_method:
#     #             df[f"wd_{i_m}"] = np.array(
#     #                 Parallel(n_jobs=num_workers, verbose=1, max_nbytes=None)(
#     #                     delayed(wd_parallel)(i, df, f"{i_m}") for i in range(len(df))
#     #                 )
#     #             )
#     #     # Aggregating metrics
#     #     aggregate_results = {"nb_filtered_constructs": len(df)}
#     #     for i_m in inference_method:
#     #         for s_s in summary_statistics:
#     #             aggregate_results[f"mape_{s_s}_{i_m}"] = df[f"ape_{s_s}_{i_m}"].mean()
#     #             aggregate_results[f"sape_{s_s}_{i_m}"] = df[f"ape_{s_s}_{i_m}"].std()
#     #         if args.wd:
#     #             aggregate_results[f"mape_wd_{i_m}"] = df[f"wd_{i_m}"].mean()
#     #             aggregate_results[f"sape_wd_{i_m}"] = df[f"wd_{i_m}"].std()
#     i = 810
#     return df["fluo_mu_mom"].iloc[i], df["fluo_mu_mle"].iloc[i], df["fluo_mu"].iloc[i]
#
#
# def main():  # noqa: CCR001
#     """Main script."""
#     # parse args
#     args = parse_args()
#     output_path = Path(args.output_path)
#     output_path.mkdir(parents=True, exist_ok=True)
#
#     # library data
#     library = pd.read_csv(Path("forecast/data") / "gamma/library_gamma.csv")
#     # Account for fluorescence-protein ratio. See derivation in sup. mat. for explanations
#     library["b"] *= args.f_amp
#     inference_method = ["mle", "mom"]
#     summary_statistics = ["a", "b", "fluo_mu", "fluo_sigma"]
#     aggregate_method = ["mape", "sape"]
#     mape_list = [
#         f"{a_m}_{s_s}_{i_m}"
#         for i_m in inference_method
#         for s_s in summary_statistics
#         for a_m in aggregate_method
#     ]
#     wd_list = [f"{a_m}_wd_{i_m}" for i_m in inference_method for a_m in aggregate_method]
#     context_list = [
#         "fmax",
#         "distribution",
#         "diversity",
#         "bias",
#         "rep",
#         "seq",
#         "bins",
#         "size",
#         "pcr_amp",
#         "f_amp",
#     ]
#     nb_new_features = len(mape_list + wd_list * args.wd)
#
#     print("yo")
#     path_simulation = Path("out") / f"{args.simulation_directory}"
#     list_mom = []
#     list_mle = []
#     true_value = 165
#
#     all_paths = list(path_simulation.glob("results_*"))
#     # few_index = len(all_paths) // 10
#     for path_result in all_paths:
#         # ctxt = path_result.stem
#         # list_context = ctxt[ctxt.find("(") + 1 : ctxt.find(")")].replace("'", "").split(",")
#         # dict_summary = process_data(
#         #     pd.read_csv(path_result), library, args.num_workers, args, nb_new_features
#         # )
#         list_mom.append(
#             process_data(
#                 pd.read_csv(path_result), library, args.num_workers, args, nb_new_features
#             )[0]
#         )
#         list_mle.append(
#             process_data(
#                 pd.read_csv(path_result), library, args.num_workers, args, nb_new_features
#             )[1]
#         )
#         true_value = process_data(
#             pd.read_csv(path_result), library, args.num_workers, args, nb_new_features
#         )[2]
#
#     # df_first = pd.DataFrame(list_data, columns=context_list + list(dict_summary.keys()))
#     # df_first.to_csv(output_path / "first.csv", index=False)
#
#     # for path_result in all_paths[few_index:]:
#     #     ctxt = path_result.stem
#     #     list_context = ctxt[ctxt.find("(") + 1 : ctxt.find(")")].replace("'", "").split(",")
#     #     dict_summary = process_data(
#     #         pd.read_csv(path_result), library, args.num_workers, args, nb_new_features
#     #     )
#     #     list_data.append(list_context + list(dict_summary.values()))
#
#     sns.distplot(list_mom)
#     sns.distplot(list_mle)
#     plt.axvline(x=true_value, ymin=0, ymax=1.25, color="black")
#     # df_summary = pd.DataFrame(list_data, columns=context_list + list(dict_summary.keys()))
#     # df_summary.to_csv(output_path / "complete.csv", index=False)
#     plt.show()
#
#
# if __name__ == "__main__":
#     main()
