"""Main script to execute inference."""
import argparse
import csv
from datetime import datetime
from pathlib import Path

import numpy as np
import pandas as pd

from forecast.protocol.inference_steps import parallel_inference
from forecast.util.choice import LIST_OF_DISTRIBUTIONS, LIST_OF_PROCEDURES
from forecast.util.experiment import Experiment


def time_created(path: Path) -> float:
    """Indicates the time the path was created as in https://www.unixtimestamp.com/ .

    Args:
        path: directory path

    Returns:
        Unix time
    """
    return path.stat().st_atime


def parse_args():
    """Parse arguments."""
    parser = argparse.ArgumentParser(description="The parent parser.", add_help=False)
    subparsers = parser.add_subparsers(title="binning actions")
    parser.add_argument(
        "--distribution",
        type=str,
        default="gamma",
        choices=LIST_OF_DISTRIBUTIONS,
        help="Fluorescence distribution name",
    )
    parser.add_argument(
        "--metadata_path",
        type=Path,
        help="Folder path containing all Flow-seq data.",
        default=str(max(list(Path("out").glob("simulation_*")), key=time_created)),
    )
    parser.add_argument(
        "--out_path",
        type=Path,
        default="out/inference_" + datetime.now().strftime("%Y%m%d-%H%M%S"),
        help="Path for the output folder.",
    )
    parser.add_argument(
        "--first_index", type=int, default=0, help="Index of first construct to infer."
    )
    parser.add_argument(
        "--last_index",
        type=str,
        default="sample",
        choices=LIST_OF_PROCEDURES,
        help="Conduct inference all the way through or just a sample?.",
    )
    parser.add_argument(
        "--num_workers", type=int, default=-1, help="Number of workers in parallel."
    )
    parser.add_argument(
        "--verbose", type=int, default=1, help="progress messages for multi-parallel processing."
    )
    # create the parser for the "automatic binning" command
    parser_a = subparsers.add_parser(
        "auto_bin",
        help="input parameters for automatic binning",
        parents=[parser],
        add_help=False,
    )
    parser_a.add_argument("--f_max", type=float, default=1e5, help="Fluorescence max of the FACS.")

    # create the parser for the "custom_bin" command
    parser_b = subparsers.add_parser(
        "custom_bin",
        help="input list of upper fluorescence boundaries. The first bin must have a fluorescence greater than 1 a.u.",
        parents=[parser],
        add_help=False,
    )
    parser_b.add_argument(
        "--upper_bounds",
        nargs="+",
        help="List of fluorescence boundaries.",
        default=["1"],
    )
    args = parser.parse_args()
    return args


def main():  # noqa: CCR001
    """Main script."""
    # parse args
    args = parse_args()
    print(args)
    out_path = args.out_path
    out_path.mkdir(parents=True, exist_ok=True)
    print("flow seq data from:", args.metadata_path)
    # Loading the number of cells falling into each bin data
    cells_bins = (
        pd.read_csv(Path(args.metadata_path) / "cells_bins.csv").to_numpy().astype(float)[0]
    )
    # Loading the sequencing data
    df_sequencing = pd.read_csv(Path(args.metadata_path) / "sequencing.csv", index_col=0)
    sequencing = df_sequencing.to_numpy().astype(int)

    # Extract information
    reads = np.sum(sequencing, axis=0)
    diversity = len(sequencing[:, 0])
    bins = int(len(sequencing[0, :]))
    # Compute the fluorescence interval
    try:
        fluo_boundary = np.array(
            [0] + [float(x) for x in args.upper_bounds]
        )  # add left edge of first bin
        f_max = fluo_boundary[-1]
    except AttributeError:
        fluo_boundary = np.logspace(0, np.log10(args.f_max), bins)
        fluo_boundary[0] = 0
        f_max = args.f_max

    # Experiment Class
    my_experiment = Experiment(
        bins=bins,
        diversity=diversity,
        nj=cells_bins,
        reads=reads,
        sequencing=sequencing,
        fmax=f_max,
        distribution=args.distribution,
        fluo_boundary=fluo_boundary,
    )

    # Get stopping index
    if args.last_index == "sample":
        last_idx = 100 + args.first_index
    else:
        last_idx = diversity
    # Conduct Inference
    df_results = parallel_inference(
        args.first_index, int(last_idx), my_experiment, args.num_workers, args.verbose
    )
    df_results = df_results.round(3)
    # Add ID column
    df_results["ID"] = list(df_sequencing.index)[:last_idx]
    df_results.set_index("ID", inplace=True)
    df_results.to_csv(args.out_path / "results.csv", index=True, index_label="ID")

    # Store flow-seq data in same folder for convenience
    (args.out_path / "cells_bins.csv").write_bytes(
        (args.metadata_path / "cells_bins.csv").read_bytes()
    )
    (args.out_path / "sequencing.csv").write_bytes(
        (args.metadata_path / "sequencing.csv").read_bytes()
    )

    a_dict = vars(args)
    with open(args.out_path / "metadata_inference.csv", "w") as a_file:
        writer = csv.writer(a_file)
        for key, value in a_dict.items():
            writer.writerow([key, value])
        writer.writerow(["fluorescence_boundaries", fluo_boundary])


if __name__ == "__main__":
    main()
