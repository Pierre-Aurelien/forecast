"""Module to define useful transformations."""
import numpy as np
import scipy.stats as stats
from scipy.stats import gamma, lognorm, norm, wasserstein_distance


def log_moments(m, s):
    """Computes the moments of a log_normal distribution.

    Args:
        m: first parameter of log-normal distribution
        s: second parameter of log-normal distribution

    Returns:
        mean of the log_normal distribution
        variance of the log_normal distribution
    """
    mean = np.exp(m + 0.5 * (s**2))
    variance = np.exp(2 * m + (s**2)) * (np.exp(s**2) - 1)
    return np.array([mean, variance])


def get_empirical_wd(distribution, x, y, x0, y0, sample_size=100000):
    """Return empirical 1-wasserstein distance between two probability distributions.

    Args:
        distribution:
        x: first parameter of the first distribution
        y: second parameter of first distribution
        x0: first parameter of the second distribution
        y0: second parameter of second distribution

    Returns:
    1-WD
    """
    if distribution == "lognormal":
        wd = wasserstein_distance(
            lognorm.rvs(s=y, scale=np.exp(x), size=sample_size),
            lognorm.rvs(s=y0, scale=np.exp(x0), size=sample_size),
        )
    elif distribution == "normal":
        wd = wasserstein_distance(
            norm.rvs(loc=x, scale=y, size=sample_size),
            norm.rvs(loc=x0, scale=y0, size=sample_size),
        )
    elif distribution == "gamma":
        wd = wasserstein_distance(
            gamma.rvs(a=x, scale=y, size=sample_size),
            gamma.rvs(a=x0, scale=y0, size=sample_size),
        )
    return wd


def get_ape(x, x_hat):
    """Computes the Absolute Percentage Error.

    Args:
        x: ground truth
        x_hat: estimates

    Returns:
        APE

    """
    return np.abs(x - x_hat) / x

    # def get_kl(distribution,x,y,x0,y0):
    # """Computes the KL divergence between two distributions: KL(Z||Z0)
    #
    # Args:
    #     distribution: distribution
    #     x: first parameter of the first distribution
    #     y: second parameter of first distribution
    #     x0: first parameter of the second distribution
    #     y0: second parameter of second distribution
    #
    # Returns:
    #     KL divergence
    # """
    # if distribution == "lognormal":
    #     # wd = wasserstein_distance(
    #     #     lognorm.rvs(s=y, scale=np.exp(x), size=sample_size),
    #     #     lognorm.rvs(s=y0, scale=np.exp(x0), size=sample_size),
    #     # )
    # elif distribution == "normal":
    #     # wd = wasserstein_distance(
    #     #     norm.rvs(loc=x, scale=y, size=sample_size),
    #     #     norm.rvs(loc=x0, scale=y0, size=sample_size),
    #     # )
    # elif distribution == "gamma":
    #     kl=
    # return kl


def visibility(m, s, fm, distribution):
    """Computes the probability mass of the distribution inside the upper bounded domain.

    Args:
        m: mean of first normal distribution
        s: variance of first normal distribution
        fm: fluorescence upper bound
        distribution: fluorescence distribution name

    Returns:
        Computes the probability mass up to fm of the normal distribution parameterised by m and s
    """
    if distribution == "lognormal":
        vis = stats.lognorm.cdf(fm, s=s, scale=np.exp(m)) - stats.lognorm.cdf(
            0, s=s, scale=np.exp(m)
        )
    elif distribution == "normal":
        vis = stats.lognorm.cdf(fm, s=s, scale=np.exp(m)) - stats.lognorm.cdf(
            0, s=s, scale=np.exp(m)
        )
    elif distribution == "gamma":
        vis = stats.gamma.cdf(fm, a=m, scale=s)
    return vis


def ab_to_ms(a, b):
    """Convert shape and scale of gamma distribution to mean and standard deviation.

    Args:
        a: shape of gamma distribution
        b: scale of gamma distribution

    Returns:
        mean and standard deviation
    """
    return np.array([a * b, np.sqrt(a) * b])


def ms_to_ab(m, s) -> np.ndarray:
    """Convert mean and standard deviation of gamma distribution to shape and scale.

    Args:
        m: mean
        s: standard deviation

    Returns:
        shape and scale parameters
    """
    # takes as input the mean and standard deviation of the gamma dsitribution and return shape and scale parameters
    shape = np.divide(m**2, s**2, out=np.zeros_like(m), where=s != 0, dtype=float)
    scale = np.divide(s**2, m, out=np.zeros_like(s), where=m != 0, dtype=float)
    return np.array([shape, scale])
