"""List of options implemented."""


LIST_OF_DISTRIBUTIONS = frozenset(["gamma", "lognormal"])
LIST_OF_BIAS = frozenset([True, False])
LIST_OF_PROCEDURES = frozenset(["sample", "all"])
