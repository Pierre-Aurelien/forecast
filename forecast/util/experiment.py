"""Module to define Experiment class."""
import numpy as np


class Experiment:
    """Class for Flow-seq experiment."""

    def __init__(self, bins, diversity, nj, reads, sequencing, fmax, distribution, fluo_boundary):
        """Init.

        Args:
            bins: number of bins
            diversity: number of different genetic constructs
            nj:FACS events in each bin ( Number of cells sorted in each bin)
            reads:#Number of reads allocated in each bin
            sequencing: Filtered Read Counts for each genetic construct
            (one row) in each bin (one column)
            fmax:fluorescence max of the FACS
            distribution:fluorescence distribution. either lognormal or gamma
            fluo_boundary:fluorescence boundaries of all bins (including the left edge of the first bin and the right edge of the last bin
            which is +infinity )
        """
        self.bins = bins
        self.diversity = diversity
        self.nj = nj
        self.size = np.sum(self.nj)
        self.reads = reads
        self.sequencing = sequencing
        self.fmax = fmax
        self.distribution = distribution
        self.fluo_boundary = fluo_boundary
        self.si = sequencing.sum(axis=1)
        if distribution == "lognormal":
            # Working in log-space
            partition = np.insert(np.log(fluo_boundary[1:]), 0, 0)
            # partition = np.log(np.logspace(0, np.log10(self.fmax), bins + 1))
        elif distribution == "gamma":
            partition = fluo_boundary
        self.partition = partition
        self.mean_assigned = [
            (self.partition[j + 1] + self.partition[j]) / 2 for j in range(bins - 1)
        ] + [self.partition[bins - 1]]
        self.partition = np.append(partition, [np.inf])
        self.enrich = np.divide(
            nj, reads, out=np.zeros_like(nj, dtype=float), where=reads != 0, dtype=float
        )
        self.nijhat = np.multiply(self.sequencing, self.enrich).astype(int)
        self.nihat = self.nijhat.sum(axis=1)
        # Normalisation of sequencing reads to control Confidence Interval width
        condition = np.repeat(
            np.sum(self.sequencing, axis=1)[:, np.newaxis] > np.array(self.nihat)[:, np.newaxis],
            self.bins,
            axis=1,
        )
        self.sequencing = self.sequencing.astype(float)
        self.sequencing = np.divide(
            self.sequencing * (np.array(self.nihat)[:, np.newaxis]),
            np.sum(self.sequencing, axis=1)[:, np.newaxis],
            out=self.sequencing,
            where=condition,
            dtype=float,
        )
        self.reads = np.sum(self.sequencing, axis=0)
