"""Main script to execute simulation (and inference)."""
import argparse
import csv
from datetime import datetime
from pathlib import Path

import numpy as np
import pandas as pd

from forecast.protocol.simulation_steps import sorting_and_sequencing
from forecast.util.choice import LIST_OF_BIAS, LIST_OF_DISTRIBUTIONS
from forecast.util.simulation import Simulation


def parse_args():
    """Parse arguments."""
    parser = argparse.ArgumentParser(description="The parent parser.", add_help=False)
    subparsers = parser.add_subparsers(title="binning actions")
    parser.add_argument(
        "--distribution",
        type=str,
        default="gamma",
        choices=LIST_OF_DISTRIBUTIONS,
        help="Fluorescence distribution name",
    )
    parser.add_argument(
        "--size", type=float, default=1e6, help="Number of bacteria sorted trough the FACS."
    )
    parser.add_argument(
        "--reads", type=float, default=1e5, help="Number of reads allocated to sequencing."
    )
    parser.add_argument(
        "--ratio_amplification", type=float, default=1e2, help="PCR amplification ratio."
    )
    parser.add_argument(
        "--bias_library",
        type=bool,
        default=False,
        choices=LIST_OF_BIAS,
        help="Bias in the library.",
    )
    parser.add_argument(
        "--metadata_path",
        type=Path,
        help="Folder path containing csv library data.",
        default="forecast/data",
    )
    parser.add_argument(
        "--out_path",
        type=Path,
        default="out/simulation_" + datetime.now().strftime("%Y%m%d-%H%M%S"),
        help="Path for the output folder.",
    )
    parser.add_argument(
        "--f_amp",
        type=int,
        default=1,
        choices=range(1, int(1e6)),
        metavar="PARAMETER",
        help="Ratio fluorescence/protein.",
    )

    # create the parser for the "automatic binning" command
    parser_a = subparsers.add_parser(
        "auto_bin",
        help="input parameters for automatic binning",
        parents=[parser],
        add_help=False,
    )
    parser_a.add_argument("--f_max", type=float, default=1e5, help="Fluorescence max of the FACS.")
    parser_a.add_argument("--bins", type=int, default=12, help="Number of bins.")

    # create the parser for the "custom_bin" command
    parser_b = subparsers.add_parser(
        "custom_bin",
        help="input list of upper fluorescence boundaries. The first bin must have a fluorescence greater than 1 a.u.",
        parents=[parser],
        add_help=False,
    )
    parser_b.add_argument(
        "--upper_bounds",
        nargs="+",
        help="List of fluorescence boundaries.",
        default=["1"],
    )
    args = parser.parse_args()
    return args


def main():  # noqa: CCR001
    """Main script."""
    # parse args
    args = parse_args()
    print(args)
    out_path = args.out_path
    out_path.mkdir(parents=True, exist_ok=True)

    # Loading library data
    df = pd.read_csv(
        Path(args.metadata_path) / f"{args.distribution}" / f"library_{args.distribution}.csv"
    )

    # Extract distribution parameters
    theta1 = df.iloc[:, 0].to_numpy()
    theta2 = df.iloc[:, 1].to_numpy()

    # Account for fluorescence-protein ratio. See derivation in sup. mat. for explanations
    if args.distribution == "gamma":
        theta2 *= args.f_amp
    elif args.distribution == "lognormal":
        theta1 += np.log(args.f_amp)

    # Compute the fluorescence interval
    try:
        fluo_boundary = np.array(
            [0] + [float(x) for x in args.upper_bounds]
        )  # add left edge of first bin
        f_max = fluo_boundary[-1]
        bins = len(fluo_boundary) - 1
    except AttributeError:
        fluo_boundary = np.logspace(
            0, np.log10(args.f_max), args.bins
        )  # removed 1 to args.bins here
        fluo_boundary[0] = 0
        bins = args.bins
        f_max = args.f_max

    # Create an instance of class simulation
    my_simulation = Simulation(
        bins=bins,
        diversity=len(theta1),
        size=args.size,
        reads=args.reads,
        fmax=f_max,
        distribution=args.distribution,
        ratio_amplification=args.ratio_amplification,
        theta1=theta1,
        theta2=theta2,
        bias_library=args.bias_library,
        fluo_boundary=fluo_boundary,
    )

    # Generate Flow-seq data
    sequencing_matrix, sorted_matrix = sorting_and_sequencing(my_simulation)
    # Add ID column
    sequencing_matrix = np.insert(sequencing_matrix, 0, np.arange(len(theta1)), axis=1)
    # Save data
    header_bin = ",".join([f"bin_{i}" for i in range(my_simulation.bins)])
    header_bin_id = ",".join(["ID"] + [f"bin_{i}" for i in range(my_simulation.bins)])
    np.savetxt(
        args.out_path / "sequencing.csv",
        sequencing_matrix,
        comments="",
        delimiter=",",
        fmt="% 4d",
        header=header_bin_id,
    )
    np.savetxt(
        args.out_path / "cells_bins.csv",
        sorted_matrix[None],
        delimiter=",",
        fmt="% 4d",
        header=header_bin,
    )
    a_dict = vars(args)
    with open(args.out_path / "metadata_simulation.csv", "w") as a_file:
        writer = csv.writer(a_file)
        for key, value in a_dict.items():
            writer.writerow([key, value])
        writer.writerow(["fluorescence_boundaries", fluo_boundary])


if __name__ == "__main__":
    main()
