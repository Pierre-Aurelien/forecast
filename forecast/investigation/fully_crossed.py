"""Main script to implement a full factorial experimental design."""
import argparse
import csv
from datetime import datetime
from pathlib import Path

import numpy as np
import pandas as pd

from forecast.protocol.inference_steps import parallel_inference
from forecast.protocol.simulation_steps import sorting_and_sequencing
from forecast.util.choice import LIST_OF_BIAS, LIST_OF_DISTRIBUTIONS, LIST_OF_PROCEDURES
from forecast.util.experiment import Experiment
from forecast.util.simulation import Simulation


def parse_args():
    """Parse arguments."""
    parser = argparse.ArgumentParser(description="Full factorial design.")
    subparsers = parser.add_subparsers(title="binning actions")
    parser.add_argument("--rep", type=int, default=1, help="Number of replicates.")
    parser.add_argument(
        "--distribution",
        type=str,
        default="gamma",
        choices=LIST_OF_DISTRIBUTIONS,
        help="Fluorescence distribution name",
    )
    parser.add_argument(
        "--reads",
        nargs="+",
        help="List: Number of reads allocated to sequencing.",
        default=["1e7"],
    )
    parser.add_argument(
        "--size",
        nargs="+",
        help="Number of cells sorted trough the FACS.",
        default=["1e6"],
    )
    parser.add_argument(
        "--ratio_amplification",
        nargs="+",
        type=float,
        default=["1e2"],
        help="PCR amplification ratio.",
    )
    parser.add_argument(
        "--bias_library",
        type=bool,
        default=False,
        choices=LIST_OF_BIAS,
        help="Bias in the library.",
    )
    parser.add_argument(
        "--metadata_path",
        type=Path,
        help="Folder path containing csv library data.",
        default="forecast/data",
    )
    parser.add_argument(
        "--output_path",
        type=Path,
        default="out/factorial_design_" + datetime.now().strftime("%Y%m%d-%H%M%S"),
        help="Path for the output folder.",
    )
    parser.add_argument(
        "--f_amp",
        type=int,
        default=["1"],
        nargs="+",
        metavar="PARAMETER",
        help="Ratio fluorescence/protein.",
    )
    parser.add_argument(
        "--first_index", type=int, default=0, help="Index of first construct to infer."
    )
    parser.add_argument(
        "--last_index",
        type=str,
        default="sample",
        choices=LIST_OF_PROCEDURES,
        help="Conduct inference all the way through or just a sample?.",
    )
    parser.add_argument(
        "--num_workers", type=int, default=-1, help="Number of workers in parallel."
    )
    parser.add_argument(
        "--verbose", type=int, default=1, help="progress messages for multi-parallel processing."
    )

    # create the parser for the "automatic binning" command
    parser_a = subparsers.add_parser(
        "auto_bin",
        help="input parameters for automatic binning",
        parents=[parser],
        add_help=False,
    )
    parser_a.add_argument("--f_max", type=float, default=1e5, help="Fluorescence max of the FACS.")
    parser_a.add_argument(
        "--bins",
        nargs="+",
        help="List: Number of bins.",
        default=["12"],
    )

    # create the parser for the "custom_bin" command
    parser_b = subparsers.add_parser(
        "custom_bin",
        help="input list of upper fluorescence boundaries. The first bin must have a fluorescence greater than 1 a.u.",
        parents=[parser],
        add_help=False,
    )
    parser_b.add_argument(
        "--upper_bounds",
        nargs="+",
        help="List of fluorescence boundaries.",
        default=["1"],
    )

    args = parser.parse_args()
    return args


def main():  # noqa: CCR001
    """Main script."""
    # parse args
    np.random.seed(325)
    args = parse_args()
    output_path = args.output_path
    output_path.mkdir(parents=True, exist_ok=True)

    # Loading library data
    df = pd.read_csv(
        Path(args.metadata_path) / f"{args.distribution}" / f"library_{args.distribution}.csv"
    )
    # Extract distribution parameters
    theta1 = df.iloc[:, 0].to_numpy()
    theta2 = df.iloc[:, 1].to_numpy()

    # Create and execute factorial range of experiments
    for rep, seq, bins, size, pcr_amp, f_amp in [
        (rep, seq, bins, size, pcr_amp, f_amp)
        for rep in range(args.rep)
        for seq in args.reads
        for bins in args.bins
        for size in args.size
        for pcr_amp in args.ratio_amplification
        for f_amp in args.f_amp
    ]:
        # Check if we haven't allocated too much sequencing
        if float(seq) > float(size) * float(pcr_amp):
            pass
        else:

            # Compute the fluorescence interval
            try:
                fluo_boundary = np.array(
                    [0] + [float(x) for x in args.upper_bounds]
                )  # add left edge of first bin
                f_max = fluo_boundary[-1]
                bins = len(fluo_boundary) - 1
            except AttributeError:
                fluo_boundary = np.logspace(0, np.log10(args.f_max), int(bins) + 1)
                fluo_boundary[0] = 0
                f_max = args.f_max

            # Account for fluorescence-protein ratio. See derivation in sup. mat. for explanations
            # We introduce t1 and t2 because theta1 and theta2 would not be updated for different loop values
            if args.distribution == "gamma":
                t2 = theta2 * float(f_amp)
                t1 = theta1
            elif args.distribution == "lognormal":
                t1 = theta1 + np.log(float(f_amp))
                t2 = theta2

            # Create an instance of class simulation
            my_simulation = Simulation(
                bins=int(bins),
                diversity=len(theta1),
                size=float(size),
                reads=float(seq),
                fmax=args.f_max,
                distribution=args.distribution,
                ratio_amplification=float(pcr_amp),
                theta1=t1,
                theta2=t2,
                bias_library=args.bias_library,
                fluo_boundary=fluo_boundary,
            )
            # Generate Flow-seq data
            sequencing_matrix, sorted_matrix = sorting_and_sequencing(my_simulation)

            # Experiment Class
            my_experiment = Experiment(
                bins=int(bins),
                diversity=len(theta1),
                nj=sorted_matrix,
                reads=np.sum(sequencing_matrix, axis=0),
                sequencing=sequencing_matrix,
                fmax=args.f_max,
                distribution=args.distribution,
                fluo_boundary=fluo_boundary,
            )
            # Get stopping index
            if args.last_index == "sample":
                last_idx = 10 + args.first_index
            elif args.last_index == "all":
                last_idx = len(theta1)

            # Conduct Inference
            df_results = parallel_inference(
                args.first_index, last_idx, my_experiment, args.num_workers, args.verbose
            )
            df_results = df_results.round(3)

            # Add ID column
            sequencing_matrix = np.insert(sequencing_matrix, 0, np.arange(len(theta1)), axis=1)
            # Save data
            header_bin = ",".join([f"bin_{i}" for i in range(my_simulation.bins)])
            header_bin_id = ",".join(["ID"] + [f"bin_{i}" for i in range(my_simulation.bins)])

            # Save data
            np.savetxt(
                args.output_path
                / f"sequencing_{f_max, args.distribution, len(theta1), args.bias_library, rep, seq, bins, size, pcr_amp, f_amp}.csv",
                sequencing_matrix,
                comments="",
                delimiter=",",
                fmt="% 4d",
                header=header_bin_id,
            )
            np.savetxt(
                args.output_path
                / f"cells_bins_{f_max, args.distribution, len(theta1), args.bias_library, rep, seq, bins, size, pcr_amp, f_amp}.csv",
                sorted_matrix[None],
                delimiter=",",
                fmt="% 4d",
                header=header_bin,
            )
            a_dict = vars(args)
            with open(
                args.output_path
                / f"metadata_simulation_{f_max, args.distribution, len(theta1), args.bias_library, rep, seq, bins, size, pcr_amp, f_amp}.csv",
                "w",
            ) as a_file:
                writer = csv.writer(a_file)
                for key, value in a_dict.items():
                    writer.writerow([key, value])
                writer.writerow(["fluorescence_boundaries", fluo_boundary])

            # Add ID column
            # df_results["ID"] = list(df_sequencing.index)[:last_idx]
            # df_results.set_index("ID", inplace=True)
            df_results.to_csv(
                output_path
                / f"results_{f_max,args.distribution,len(theta1),args.bias_library,rep,seq,bins,size,pcr_amp,f_amp}.csv",
                index=False,
            )


if __name__ == "__main__":
    main()
