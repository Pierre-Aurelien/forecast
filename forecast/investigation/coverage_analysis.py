"""Main script to study coverage for different estimators."""
import argparse
from datetime import datetime
from pathlib import Path

import numpy as np
import pandas as pd

from forecast.protocol.inference_steps import parallel_inference
from forecast.protocol.simulation_steps import sorting_and_sequencing
from forecast.util.choice import LIST_OF_BIAS, LIST_OF_DISTRIBUTIONS, LIST_OF_PROCEDURES
from forecast.util.experiment import Experiment
from forecast.util.simulation import Simulation


def parse_args():
    """Parse arguments."""
    parser = argparse.ArgumentParser(description="Impact of sequencing step.")
    parser.add_argument("--rep", type=int, default=500, help="Number of repetitions.")
    parser.add_argument("--f_max", type=float, default=1e5, help="Fluorescence max of the FACS.")
    parser.add_argument(
        "--distribution",
        type=str,
        default="gamma",
        choices=LIST_OF_DISTRIBUTIONS,
        help="Fluorescence distribution name",
    )
    parser.add_argument("--bins", type=int, default=8, help="Number of bins.")
    parser.add_argument(
        "--size",
        nargs="+",
        help="List: Number of bacteria sorted trough the FACS..",
        default=[1e5, 1e6, 1e7],
    )
    parser.add_argument(
        "--reads",
        nargs="+",
        help="List: Number of reads allocated to sequencing.",
        default=[1e5, 1e6, 1e7],
    )

    parser.add_argument(
        "--ratio_amplification", type=int, default=1e3, help="PCR amplification ratio."
    )
    parser.add_argument(
        "--bias_library",
        type=bool,
        default=False,
        choices=LIST_OF_BIAS,
        help="Bias in the library.",
    )
    parser.add_argument(
        "--metadata_path",
        type=Path,
        help="Folder path containing csv library data.",
        default="forecast/data",
    )
    parser.add_argument(
        "--output_path",
        type=Path,
        default="out/pcr_impact/simulation_" + datetime.now().strftime("%Y%m%d-%H%M%S"),
        help="Path for the output folder.",
    )
    parser.add_argument(
        "--f_amp",
        type=int,
        default=10,
        choices=range(1, int(1e6)),
        metavar="PARAMETER",
        help="Ratio fluorescence/protein.",
    )
    parser.add_argument(
        "--first_index", type=int, default=0, help="Index of first construct to infer."
    )
    parser.add_argument(
        "--end_index",
        type=str,
        default="sample",
        choices=LIST_OF_PROCEDURES,
        help="Conduct inference all the way through or just a sample?.",
    )
    parser.add_argument("--index", type=int, default=0, help="Construct characterised.")
    parser.add_argument(
        "--num_workers", type=int, default=-1, help="Number of workers in parallel."
    )
    parser.add_argument(
        "--verbose", type=int, default=1, help="progress messages for multi-parallel processing."
    )
    args = parser.parse_args()
    return args


# df_truth=pd.read_csv('library_gamma.csv')
# df_truth["b"] = 10 * df_truth['b']
# ground_truth=df_truth.to_numpy()[:10,:]
# ground_truth


# def plot_coverage(probit, construct, raw_data, theta, filtering):
#     #Filter out weird constructs
#     raw_data
#
#
#     coverage_counter = 0
#     fig, ax = plt.subplots(1, 1, figsize=(6, 8))
#     number_trials = int(len(raw_data) / filtering)
#     y_coord = np.arange(number_trials)
#     for i in range(number_trials):
#         if np.abs(ground_truth[construct, theta] - raw_data[i, construct, theta]) <= probit * raw_data[
#             i, construct, 2 + theta]:
#             # interval contains p
#             ax.errorbar(raw_data[i, construct, theta], y_coord[i], lolims=True,
#                         xerr=probit * raw_data[i, construct, 2 + theta], yerr=0.0, linestyle='', c='grey')
#             coverage_counter += 1
#         else:
#             # interval does not contain p
#             ax.errorbar(raw_data[i, construct, theta], y_coord[i], lolims=True,
#                         xerr=probit * raw_data[i, construct, 2 + theta], yerr=0.0, linestyle='', c='tab:red')
#     ax.axvline(ground_truth[construct, theta], color='black')
#     plt.ylabel('Simulation ', fontsize=16)
#     plt.xlabel('parameter value', fontsize=20)
#     plt.title(f'{coverage_counter}/{number_trials}={np.round(coverage_counter / number_trials, 2)}')
#
#     custom = [Line2D([], [], marker='.', markersize=20, color='grey', linestyle='None'),
#               Line2D([], [], marker='.', markersize=20, color='tab:red', linestyle='None')]
#     plt.legend(handles=custom, title='Parameter in CI', labels=['Yes', 'No'], loc='upper right', fontsize=10)
#     print(f'{coverage_counter}/{number_trials}={np.round(coverage_counter / number_trials, 2)}')
# #     return fig


def main():  # noqa: CCR001
    """Main script."""
    # parse args
    args = parse_args()
    output_path = args.output_path

    output_path.mkdir(parents=True, exist_ok=True)
    df = pd.read_csv(
        Path(args.metadata_path) / f"{args.distribution}" / f"library_{args.distribution}.csv"
    )

    # Extract distribution parameters
    theta1 = df.iloc[:, 0].to_numpy()
    theta2 = df.iloc[:, 1].to_numpy()

    # Account for fluorescence-protein ratio. See derivation in sup. mat. for explanations
    if args.distribution == "gamma":
        theta2 *= args.f_amp
    elif args.distribution == "lognormal":
        theta1 += np.log(args.f_amp)

    fluo_boundary = np.logspace(0, np.log10(args.f_max), args.bins + 1)
    fluo_boundary[0] = 0

    # Create and execute factorial range of experiments
    for (
        seq,
        size,
    ) in [(seq, size) for seq in args.reads for size in args.size]:
        a_matrix = np.empty((args.rep, 6, 3))
        b_matrix = np.empty((args.rep, 6, 3))
        for rep in range(args.rep):
            # Generate Flow-seq data
            # Create an instance of class simulation
            my_simulation = Simulation(
                bins=args.bins,
                diversity=len(theta1),
                size=float(size),
                reads=float(seq),
                fmax=args.f_max,
                distribution=args.distribution,
                ratio_amplification=float(args.ratio_amplification),
                theta1=theta1,
                theta2=theta2,
                bias_library=args.bias_library,
                fluo_boundary=fluo_boundary,
            )
            # Generate Flow-seq data
            sequencing_matrix, sorted_matrix = sorting_and_sequencing(my_simulation)

            # Experiment Class
            my_experiment = Experiment(
                bins=args.bins,
                diversity=len(theta1),
                nj=sorted_matrix,
                reads=np.sum(sequencing_matrix, axis=0),
                sequencing=sequencing_matrix,
                fmax=args.f_max,
                distribution=args.distribution,
                fluo_boundary=fluo_boundary,
            )

            # Get stopping index
            if args.end_index == "sample":
                last_idx = 6
            else:
                last_idx = len(theta1)

            df = parallel_inference(
                args.first_index, last_idx, my_experiment, args.num_workers, args.verbose
            )
            a_matrix[rep, :, :] = df.loc[:6, ["a_mle", "a_se", "inference_grade"]].to_numpy()
            b_matrix[rep, :, :] = df.loc[:6, ["b_mle", "b_se", "inference_grade"]].to_numpy()

        np.save(f"a_seq_size_{seq,size}", a_matrix)
        np.save(f"b_seq_size_{seq, size}", b_matrix)


if __name__ == "__main__":
    main()
