"""Main script to study impact of sampling variability (CI coverage for instance)."""
import argparse
from datetime import datetime
from pathlib import Path

import numpy as np
import pandas as pd

from forecast.protocol.inference_steps import parallel_inference
from forecast.protocol.simulation_steps import sorting_and_sequencing
from forecast.util.choice import LIST_OF_BIAS, LIST_OF_DISTRIBUTIONS, LIST_OF_PROCEDURES
from forecast.util.experiment import Experiment
from forecast.util.simulation import Simulation


def parse_args():
    """Parse arguments."""
    parser = argparse.ArgumentParser(description="Impact of sequencing step.")
    parser.add_argument("--rep", type=int, default=10, help="Number of repetitions.")
    parser.add_argument("--f_max", type=float, default=1e5, help="Fluorescence max of the FACS.")
    parser.add_argument(
        "--distribution",
        type=str,
        default="gamma",
        choices=LIST_OF_DISTRIBUTIONS,
        help="Fluorescence distribution name",
    )
    parser.add_argument("--bins", type=int, default=8, help="Number of bins.")
    parser.add_argument(
        "--size", type=float, default=1e9, help="Number of bacteria sorted trough the FACS."
    )
    parser.add_argument(
        "--reads", type=float, default=1e7, help="Number of reads allocated to sequencing."
    )
    parser.add_argument(
        "--ratio_amplification", type=int, default=100, help="PCR amplification ratio."
    )
    parser.add_argument(
        "--bias_library",
        type=bool,
        default=False,
        choices=LIST_OF_BIAS,
        help="Bias in the library.",
    )
    parser.add_argument(
        "--metadata_path",
        type=Path,
        help="Folder path containing csv library data.",
        default="forecast/data",
    )
    parser.add_argument(
        "--output_path",
        type=Path,
        default="out/pcr_impact/simulation_" + datetime.now().strftime("%Y%m%d-%H%M%S"),
        help="Path for the output folder.",
    )
    parser.add_argument(
        "--f_amp",
        type=int,
        default=10,
        choices=range(1, int(1e6)),
        metavar="PARAMETER",
        help="Ratio fluorescence/protein.",
    )
    parser.add_argument(
        "--first_index", type=int, default=0, help="Index of first construct to infer."
    )
    parser.add_argument(
        "--end_index",
        type=str,
        default="sample",
        choices=LIST_OF_PROCEDURES,
        help="Conduct inference all the way through or just a sample?.",
    )
    parser.add_argument(
        "--num_workers", type=int, default=-1, help="Number of workers in parallel."
    )
    parser.add_argument(
        "--verbose", type=int, default=1, help="progress messages for multi-parallel processing."
    )
    args = parser.parse_args()
    return args


def main():  # noqa: CCR001
    """Main script."""
    # parse args
    args = parse_args()
    output_path = args.output_path

    output_path.mkdir(parents=True, exist_ok=True)
    df = pd.read_csv(
        Path(args.metadata_path) / f"{args.distribution}" / f"library_{args.distribution}.csv"
    )

    # Extract distribution parameters
    theta1 = df.iloc[:, 0].to_numpy()
    theta2 = df.iloc[:, 1].to_numpy()

    # Account for fluorescence-protein ratio. See derivation in sup. mat. for explanations
    if args.distribution == "gamma":
        theta2 *= args.f_amp
    elif args.distribution == "lognormal":
        theta1 += np.log(args.f_amp)

    fluo_boundary = np.logspace(0, np.log10(args.f_max), args.bins + 1)
    fluo_boundary[0] = 0

    # Create an instance of class simulation
    my_simulation = Simulation(
        bins=args.bins,
        diversity=len(theta1),
        size=args.size,
        reads=args.reads,
        fmax=args.f_max,
        distribution=args.distribution,
        ratio_amplification=args.ratio_amplification,
        theta1=theta1,
        theta2=theta2,
        bias_library=args.bias_library,
        fluo_boundary=fluo_boundary,
    )
    # Execute range of experiments
    for rep in range(args.rep):
        # Generate Flow-seq data
        sequencing_matrix, sorted_matrix = sorting_and_sequencing(my_simulation)
        # Save data
        np.savetxt(
            args.output_path / f"sequencing_rep_{rep}.csv",
            sequencing_matrix,
            comments="",
            delimiter=",",
        )
        np.savetxt(
            args.output_path / f"cells_bins_rep_{rep}.csv", sorted_matrix[None], delimiter=","
        )
    # Conduct inference on the data from the above range of experiments
    for rep in range(args.rep):
        # Loading the number of cells falling into each bin data
        cells_bins = (
            pd.read_csv(Path(args.output_path) / f"cells_bins_rep_{rep}.csv", header=None)
            .to_numpy()
            .astype(float)[0]
        )
        # Loading the sequencing data
        sequencing = (
            pd.read_csv(Path(args.output_path) / f"sequencing_rep_{rep}.csv", header=None)
            .to_numpy()
            .astype(int)
        )
        # Extract information
        reads = np.sum(sequencing, axis=0)
        diversity = len(sequencing[:, 0])
        bins = int(len(sequencing[0, :]))
        output_path.mkdir(parents=True, exist_ok=True)
        # Experiment Class
        my_experiment = Experiment(
            bins=bins,
            diversity=diversity,
            nj=cells_bins,
            reads=reads,
            sequencing=sequencing,
            fmax=args.f_max,
            distribution=args.distribution,
            fluo_boundary=fluo_boundary,
        )
        # Get stopping index
        if args.end_index == "sample":
            last_idx = 10
        else:
            last_idx = diversity

        parallel_inference(
            args.first_index, last_idx, my_experiment, args.num_workers, args.verbose
        ).to_csv(output_path / f"results_rep_{rep}.csv", index=False)


if __name__ == "__main__":
    main()
