"""Main script to visualise likelihood and information."""
import argparse
from datetime import datetime
from pathlib import Path

import matplotlib
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from joblib import Parallel, delayed
from mpl_toolkits.mplot3d import Axes3D

from forecast.protocol.inference_steps import neg_ll, parallel_inference
from forecast.protocol.simulation_steps import sorting_and_sequencing
from forecast.util.choice import LIST_OF_BIAS, LIST_OF_DISTRIBUTIONS, LIST_OF_PROCEDURES
from forecast.util.experiment import Experiment
from forecast.util.simulation import Simulation


def parse_args():
    """Parse arguments."""
    parser = argparse.ArgumentParser(description="Impact of sequencing step.")
    parser.add_argument("--f_max", type=float, default=1e5, help="Fluorescence max of the FACS.")
    parser.add_argument(
        "--distribution",
        type=str,
        default="gamma",
        choices=LIST_OF_DISTRIBUTIONS,
        help="Fluorescence distribution name",
    )
    parser.add_argument("--bins", type=int, default=8, help="Number of bins.")
    parser.add_argument(
        "--size", type=float, default=6e5, help="Number of bacteria sorted trough the FACS."
    )
    parser.add_argument(
        "--reads", type=float, default=3e5, help="Number of reads allocated to sequencing."
    )
    parser.add_argument(
        "--ratio_amplification", type=int, default=1000, help="PCR amplification ratio."
    )
    parser.add_argument(
        "--bias_library",
        type=bool,
        default=False,
        choices=LIST_OF_BIAS,
        help="Bias in the library.",
    )
    parser.add_argument(
        "--metadata_path",
        type=Path,
        help="Folder path containing csv library data.",
        default="forecast/data",
    )
    parser.add_argument(
        "--output_path",
        type=Path,
        default="out/pcr_impact/simulation_" + datetime.now().strftime("%Y%m%d-%H%M%S"),
        help="Path for the output folder.",
    )
    parser.add_argument(
        "--f_amp",
        type=int,
        default=10,
        choices=range(1, int(1e6)),
        metavar="PARAMETER",
        help="Ratio fluorescence/protein.",
    )
    parser.add_argument(
        "--first_index", type=int, default=0, help="Index of first construct to infer."
    )
    parser.add_argument(
        "--end_index",
        type=str,
        default="sample",
        choices=LIST_OF_PROCEDURES,
        help="Conduct inference all the way through or just a sample?.",
    )
    parser.add_argument(
        "--num_workers", type=int, default=-1, help="Number of workers in parallel."
    )
    parser.add_argument(
        "--verbose", type=int, default=1, help="progress messages for multi-parallel processing."
    )
    args = parser.parse_args()
    return args


def main():  # noqa: CCR001
    """Main script."""
    # parse args
    args = parse_args()
    # output_path = args.output_path
    #
    # output_path.mkdir(parents=True, exist_ok=True)
    df = pd.read_csv(
        Path(args.metadata_path) / f"{args.distribution}" / f"library_{args.distribution}.csv"
    )

    # Extract distribution parameters
    theta1 = df.iloc[:, 0].to_numpy()
    theta2 = df.iloc[:, 1].to_numpy()
    # Account for fluorescence-protein ratio. See derivation in sup. mat. for explanations
    if args.distribution == "gamma":
        theta2 *= args.f_amp
    elif args.distribution == "lognormal":
        theta1 += np.log(args.f_amp)

    fluo_boundary = np.logspace(0, np.log10(args.f_max), args.bins + 1)
    fluo_boundary[0] = 0

    my_simulation = Simulation(
        bins=args.bins,
        diversity=len(theta1),
        size=args.size,
        reads=args.reads,
        fmax=args.f_max,
        distribution=args.distribution,
        ratio_amplification=args.ratio_amplification,
        theta1=theta1,
        theta2=theta2,
        bias_library=args.bias_library,
        fluo_boundary=fluo_boundary,
    )

    # Generate Flow-seq data
    sequencing_matrix, sorted_matrix = sorting_and_sequencing(my_simulation)

    my_experiment = Experiment(
        bins=int(args.bins),
        diversity=len(theta1),
        nj=sorted_matrix,
        reads=np.sum(sequencing_matrix, axis=0),
        sequencing=sequencing_matrix,
        fmax=args.f_max,
        distribution=args.distribution,
        fluo_boundary=fluo_boundary,
    )

    df = parallel_inference(args.first_index, 2, my_experiment, args.num_workers, args.verbose)
    print(df)
    a_mle = df.iloc[1, 0]
    b_mle = df.iloc[1, 1]
    neg_ll_mle = neg_ll((a_mle, b_mle), 1, my_experiment)
    a_mom = df.iloc[1, -2]
    b_mom = df.iloc[1, -1]
    neg_ll_mom = neg_ll((a_mom, b_mom), 1, my_experiment)
    neg_ll_true = neg_ll((theta1[1], theta2[1]), 1, my_experiment)

    nb_x, nb_y = 100, 100
    map_theta = [
        (x, y) for y in np.linspace(10, 200, nb_y) for x in np.linspace(1, 8, nb_x)
    ]  # list(zip(np.linspace(1,10,500),np.linspace(10,80,500)))
    surface_neg_ll_results = Parallel(n_jobs=-1, verbose=1, max_nbytes=None)(
        delayed(neg_ll)(theta, 1, my_experiment) for theta in map_theta
    )

    surface_neg_ll = np.array(surface_neg_ll_results)
    zz = np.reshape(surface_neg_ll, (nb_y, nb_x))

    xx, yy = np.meshgrid(np.linspace(1, 8, nb_x), np.linspace(10, 200, nb_y))

    # Create the figure
    cm = 1 / 2.54  # centimeters in inches
    fig = plt.figure(figsize=(6.5 * cm, 6.5 * cm))
    matplotlib.rcParams["pdf.fonttype"] = 42
    matplotlib.rcParams["ps.fonttype"] = 42
    plt.rcParams.update({"font.size": 6})
    # fig = plt.figure()
    # Add an axes
    ax = Axes3D(fig, computed_zorder=False)
    surf = ax.plot_surface(
        xx, yy, zz, rstride=4, cstride=4, cmap="cividis", edgecolor="none", alpha=0.9
    )
    ax.scatter(a_mle, b_mle, neg_ll_mle, c="grey", zorder=500, s=10)
    ax.scatter(a_mom, b_mom, neg_ll_mom, c="tab:blue", zorder=500, s=10, label="MOM")
    ax.scatter(theta1[1], theta2[1], neg_ll_true, c="gold", zorder=500, s=10, label="True value")
    ax.contourf(xx, yy, zz, zdir="zz", offset=np.min(zz) - 200, cmap="cividis", alpha=0.4)

    fig.colorbar(surf, ax=ax, shrink=0.5, location="left")
    ax.set_xlabel("shape")
    ax.set_ylabel("scale")
    # ax.set_zlabel('NLL')
    ax.set_zlim(np.min(zz) - 200, np.max(zz))
    print(theta1[1], theta2[1], neg_ll_true)
    print(a_mle, b_mle, neg_ll_mle)
    print(a_mom, b_mom, neg_ll_mom)

    # Add legend with proxy artists
    col1_patch = mpatches.Patch(color="grey", label="ML")
    col2_patch = mpatches.Patch(color="tab:blue", label="MOM")
    col3_patch = mpatches.Patch(color="gold", label="True value")
    plt.legend(handles=[col1_patch, col2_patch, col3_patch], loc="upper right")
    ax.view_init(30, 510)
    plt.xlabel("shape", labelpad=-10)
    plt.ylabel("scale", labelpad=-10)
    ax.tick_params(axis="x", which="major", pad=-5)
    ax.tick_params(axis="y", which="major", pad=-5)
    ax.tick_params(axis="z", which="major", pad=-1)
    plt.savefig("nll.pdf", transparent=True, bbox_inches="tight")
    # plt.show()


if __name__ == "__main__":
    main()
