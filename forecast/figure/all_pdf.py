"""Main script to plot the pdf with the binned sequencing data."""
import argparse
from datetime import datetime
from pathlib import Path

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import scipy.stats as stats
import seaborn as sns
from matplotlib import ticker

from forecast.util.experiment import Experiment


def time_created(path: Path) -> float:
    """Indicates the time the path was created as in https://www.unixtimestamp.com/ .

    Args:
        path: directory path

    Returns:
        Unix time
    """
    return path.stat().st_atime


def parse_args():
    """Parse arguments."""
    parser = argparse.ArgumentParser(description="The parent parser.", add_help=False)
    subparsers = parser.add_subparsers(title="binning actions")
    parser.add_argument(
        "--distribution", type=str, default="gamma", help="Fluorescence distribution name"
    )
    parser.add_argument(
        "--metadata_path",
        type=Path,
        help="Folder path containing all result files.",
        default=str(max(list(Path("out").glob("inference_*")), key=time_created)),
    )
    parser.add_argument(
        "--library_path",
        type=Path,
        help="path for the ground truth csv library.(library_gamma/lognormal.csv)",
        default="forecast/data",
    )
    parser.add_argument(
        "--construct",
        type=int,
        default=0,
        help="construct index.",
    )
    parser.add_argument(
        "--out_path",
        type=Path,
        default="out/figure_pdf_" + datetime.now().strftime("%Y%m%d-%H%M%S"),
        help="Path for the output folder.",
    )
    parser.add_argument(
        "--f_amp",
        type=int,
        default=10,
        choices=range(1, int(1e6)),
        metavar="PARAMETER",
        help="Ratio fluorescence/protein.",
    )
    parser.add_argument(
        "--resolution",
        type=int,
        default=int(1e6),
        help="Number of points used to generate the plot.",
    )
    parser.add_argument(
        "--legend_loc",
        type=str,
        default="right",
        help="location of plot legend.",
    )
    # create the parser for the "automatic binning" command
    parser_a = subparsers.add_parser(
        "auto_bin",
        help="input parameters for automatic binning",
        parents=[parser],
        add_help=False,
    )
    parser_a.add_argument("--f_max", type=float, default=1e5, help="Fluorescence max of the FACS.")

    # create the parser for the "custom_bin" command
    parser_b = subparsers.add_parser(
        "custom_bin",
        help="input list of upper fluorescence boundaries. The first bin must have a fluorescence greater than 1 a.u.",
        parents=[parser],
        add_help=False,
    )
    parser_b.add_argument(
        "--upper_bounds",
        nargs="+",
        help="List of fluorescence boundaries.",
        default=["1"],
    )
    args = parser.parse_args()
    return args


def data_transformation_bins(
    i, my_experiment
):  # New representation of the data enabling the method of moments
    """Get data as samples.

    Args:
        i:index number
        my_experiment: experiment class

    Returns:
        all individual experiments.
    """
    x = my_experiment.nijhat[i, :]
    x = x.astype(int)
    if my_experiment.distribution == "gamma":
        t = np.repeat(my_experiment.mean_assigned, x)
    elif my_experiment.distribution == "lognormal":
        t = np.repeat(np.exp(my_experiment.mean_assigned), x)
    return t


def get_pdf(x, loc, scale, my_experiment):
    """Returns appropriate pdf method.

    Args:
        x: variable
        loc: first distribution parameter
        scale: second distribution parameter
        my_experiment: experiment class

    we use the standardised structure as explained in :
        https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.lognorm.html
        and
        https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.gamma.html
    Returns:
        scipy continuous random variable object.
    """
    if my_experiment.distribution == "gamma":
        pdf_method = stats.gamma.pdf(x, loc, scale=scale)
    elif my_experiment.distribution == "lognormal":
        pdf_method = stats.lognorm.pdf(x, s=scale, scale=np.exp(loc))
    return pdf_method


def main():
    """Main script."""
    args = parse_args()
    out_path = args.out_path
    out_path.mkdir(parents=True, exist_ok=True)
    df = pd.read_csv(Path(args.metadata_path) / "results.csv")

    if args.distribution == "gamma":
        name_library = "library_gamma.csv"
        theta1 = "a"
        theta2 = "b"
    elif args.distribution == "lognormal":
        name_library = "library_lognormal.csv"
        theta1 = "mu"
        theta2 = "sigma"

    df_truth = pd.read_csv(Path(args.library_path) / args.distribution / f"{name_library}")
    if args.distribution == "gamma":
        df_truth.iloc[:, 1] *= args.f_amp
    elif args.distribution == "lognormal":
        df_truth.iloc[:, 0] += np.log(args.f_amp)

    df = pd.concat([df_truth, df], axis=1)

    cells_bins = (
        pd.read_csv(Path(args.metadata_path) / "cells_bins.csv").to_numpy().astype(float)[0]
    )
    sequencing = (
        pd.read_csv(Path(args.metadata_path) / "sequencing.csv", index_col=0).to_numpy().astype(int)
    )
    reads = np.sum(sequencing, axis=0)
    diversity = len(sequencing[:, 0])
    bins = int(len(sequencing[0, :]))
    out_path.mkdir(parents=True, exist_ok=True)

    # Compute the fluorescence interval
    try:
        fluo_boundary = np.array(
            [0] + [float(x) for x in args.upper_bounds]
        )  # add left edge of first bin
        f_max = fluo_boundary[-1]
    except AttributeError:
        fluo_boundary = np.logspace(0, np.log10(args.f_max), bins)
        fluo_boundary[0] = 0
        f_max = args.f_max

    # Experiment Class
    my_experiment = Experiment(
        bins=bins,
        diversity=diversity,
        nj=cells_bins,
        reads=reads,
        sequencing=sequencing,
        fmax=f_max,
        distribution=args.distribution,
        fluo_boundary=fluo_boundary,
    )
    i = args.construct
    x = np.linspace(1, f_max, args.resolution)
    a = df.loc[i, f"{theta1}_mle"]
    b = df.loc[i, f"{theta2}_mle"]
    a2 = df.loc[i, f"{theta1}_mom"]
    b2 = df.loc[i, f"{theta2}_mom"]
    a_truth = df.iloc[i, 0]
    b_truth = df.iloc[i, 1]
    # Data
    y_mle = get_pdf(x, a, b, my_experiment)
    y_mom = get_pdf(x, a2, b2, my_experiment)
    y_truth = get_pdf(x, a_truth, b_truth, my_experiment)

    # Graph
    # For paper figures
    matplotlib.rcParams["pdf.fonttype"] = 42
    matplotlib.rcParams["ps.fonttype"] = 42
    plt.rcParams.update({"font.size": 8})
    cm = 1 / 2.54  # centimeters in inches
    fig, ax = plt.subplots(1, 1, figsize=(7.5 * cm, 7.5 * cm))
    # for presentations
    # matplotlib.rcParams["lines.linewidth"] = 3
    # matplotlib.rcParams["legend.fontsize"] = 16
    # matplotlib.rcParams["legend.markerscale"] = 2
    #
    # fig, ax = plt.subplots(figsize=(12, 8))

    # Twin the x-axis twice to make independent y-axes.
    axes = [ax, ax.twinx(), ax.twinx(), ax.twinx()]

    # Make some space on the right side for the extra y-axis.
    fig.subplots_adjust(right=0.75)

    # Move the other y-axis spine over to the right
    axes[2].spines["right"].set_position(("axes", 1.1))  # 1.27
    axes[3].spines["right"].set_position(("axes", 1.2))  # 1.52

    # To make the border of the right-most axis visible, we need to turn the frame
    # on. This hides the other plots, however, so we need to turn its fill off.
    axes[-1].set_frame_on(True)
    axes[-1].patch.set_visible(False)
    # And finally we get to plot things...
    # my_colors = ["tan", "tab:brown", "#4f7942", "black"]
    my_colors = ["wheat", "tab:grey", sns.color_palette("Blues_d")[2], "black"]
    # my_colors = ["tab:grey", "tab:red", "tab:blue", "black"]
    axes[0].hist(
        data_transformation_bins(i, my_experiment),
        bins=my_experiment.fluo_boundary,
        zorder=0,
        color=my_colors[0],
        label="Read count",
    )
    axes[1].plot(x, y_mle, label="ML inference", zorder=5, c=my_colors[1], linewidth=3)
    axes[2].plot(x, y_mom, label="MoM inference", zorder=3, c=my_colors[2], linewidth=2)
    axes[3].plot(x, y_truth, label="Ground truth", zorder=10, c=my_colors[3], linestyle="dashed")
    axes[0].set_ylabel("Estimated cell number per bin")
    axes[1].set_ylabel("pdf -ML")
    axes[2].set_ylabel("pdf - MoM")
    axes[3].set_ylabel("pdf - Ground truth")

    if args.legend_loc == "right":
        tuple_legend = (0.40, 0.74)
    elif args.legend_loc == "left":
        tuple_legend = (0.10, 0.74)

    fig.legend(loc=tuple_legend, frameon=False)
    axes[1].tick_params(axis="y", colors=my_colors[1])
    axes[2].tick_params(axis="y", colors=my_colors[2])
    axes[3].tick_params(axis="y", colors=my_colors[3])
    axes[0].set_xlabel("Fluorescence (a.u.)")

    formatter = ticker.ScalarFormatter(useMathText=True)
    formatter.set_scientific(True)
    formatter.set_powerlimits((0, 0))
    # axes[0].yaxis.set_major_formatter(formatter)
    axes[1].yaxis.set_major_formatter(formatter)
    axes[2].yaxis.set_major_formatter(formatter)
    axes[3].yaxis.set_major_formatter(formatter)
    axes[1].get_yaxis().get_offset_text().set_position((1.23, 0))
    axes[2].get_yaxis().get_offset_text().set_position((1.5, 0))
    axes[3].get_yaxis().get_offset_text().set_position((1.75, 0))

    # plt.title(f"sd_mom{ab_to_ms(a2,b2)[1]}, cst={i},sd_truth={ab_to_ms(a_truth,b_truth)[1]}")
    plt.xscale("log")
    # print(a, b, a2, b2)
    plt.savefig(
        Path(out_path) / "pdf_with_binned_reads.pdf",
        transparent=True,
        bbox_inches="tight",
        dpi=600,
    )


if __name__ == "__main__":
    main()
