"""Setup script for python packaging."""
import site
import sys

from setuptools import find_packages, setup

# enable installing package for user
# https://github.com/pypa/pip/issues/7953#issuecomment-645133255
site.ENABLE_USER_SITE = "--user" in sys.argv[1:]

setup(
    name="forecast",
    version="0.1.0",
    description="",
    author="Pierre-Aurelien Gilliot",
    zip_safe=False,
    packages=find_packages(
        where=["forecast"],
    ),
    entry_points={
        "console_scripts": [
            "infer=forecast.run_inference:main",
            "generate=forecast.run_simulation:main",
            "plot_ci=forecast.figure.ci_coverage:main",
            "pcr_ci=forecast.investigation.pcr_coverage:main",
            "plot_pdf=forecast.figure.pdf_bins:main",
            "plot_all=forecast.figure.all_pdf:main",
            "sampling_variability=forecast.investigation.sampling_variability:main",
            "likelihood=forecast.investigation.likelihood:main",
            "sequencing_impact=forecast.investigation.sequencing_impact:main",
            "factorial=forecast.investigation.fully_crossed:main",
            "extract=forecast.analysis.extracting_range:main",
            "extract_log=forecast.analysis.extracting_range_lognormal:main",
        ]
    },
)
